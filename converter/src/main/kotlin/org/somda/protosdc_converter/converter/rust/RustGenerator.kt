package org.somda.protosdc_converter.converter.rust

import com.akuleshov7.ktoml.Toml
import kotlinx.serialization.serializer
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.BaseNodePlugin
import org.somda.protosdc_converter.converter.ConverterConfig
import org.somda.protosdc_converter.converter.requireLegalFolderContents
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintWriter

const val RUST_FILE_EXTENSION: String = "rs"
const val MAPPING_TYPES_MODULE_PATH: String = "crate::types"

@kotlinx.serialization.Serializable
data class RustConfig(
    val rustModule: String,
    val outputFolder: String = "rust/",
    val outputFile: String = "out.rs",
    val protoModelModule: String,
) {
    init {
        require(!rustModule.contains("."))
        require(outputFolder.endsWith("/"))
        require(outputFile.endsWith(".$RUST_FILE_EXTENSION"))
    }
}

class RustGenerator(
    private val tree: BaseNode,
    private val converterConfig: ConverterConfig,
    private val config: RustConfig,
    private val generateCustomTypes: Boolean = true,
    private val rustBase: RustBase
) : BaseNodePlugin {

    companion object : Logging {

        private const val TOML_TABLE_NAME = "Rust"
        const val INDENT = "    "
        private fun idt(level: Int) = INDENT.repeat(level)

        private const val ONE_OF_DERIVES =
            "#[derive(Clone, PartialEq, Debug, ::protosdc_mapping_derive::protoSDCOneOf)]"
        private const val ENUM_DERIVES =
            "#[derive(Clone, PartialEq, Debug, ::protosdc_mapping_derive::protoSDCEnumeration)]"
        private const val STRUCT_DERIVES = "#[derive(Clone, PartialEq, Debug, ::protosdc_mapping_derive::protoSDCType)]"
        private const val STRUCT_DERIVES_DEFAULT =
            "#[derive(Clone, PartialEq, Debug, Default, ::protosdc_mapping_derive::protoSDCType)]"
        private const val STRUCT_ALLOWS = "#[allow(non_camel_case_types)]"
        private const val MODULE_SUFFIX = "Mod"
        const val MOD_SEP = "::"
        private const val SUPER = "super$MOD_SEP"

        private fun protoArgs(vararg parameter: String) = "#[protosdc(${parameter.joinToString(separator = ", ")})]"

        // type format
        fun rustOption(data: String) = "::core::option::Option<$data>"
        fun rustVec(data: String) = "::std::vec::Vec<$data>"
        fun rustBox(data: String) = "::std::boxed::Box<$data>"

        // struct format
        fun rustStructDerive(level: Int, default: Boolean = false) =
            "${idt(level)}${if (default) STRUCT_DERIVES_DEFAULT else STRUCT_DERIVES}"

        fun rustStructProtoParams(level: Int, protoType: String) =
            "${idt(level)}${protoArgs("proto_type = \"$protoType\"")}"

        fun rustStructAllow(level: Int) = "${idt(level)}$STRUCT_ALLOWS"
        fun rustStructStart(level: Int, data: String) = "${idt(level)}pub struct $data {"
        fun rustStructEnd(level: Int) = "${idt(level)}}"

        // field format
        fun rustStructField(
            level: Int,
            annotation: String?,
            parameterName: String,
            parameterType: String,
            optional: Boolean
        ): String {
            val pType = when (optional) {
                true -> rustOption(parameterType)
                false -> parameterType
            }

            return "${annotation?.let { "${idt(level)}$it\n" } ?: ""}${idt(level)}pub $parameterName: $pType,"
        }

        // enum format
        fun rustEnumDerive(level: Int) = "${idt(level)}$ENUM_DERIVES"

        fun rustEnumProtoParams(level: Int, protoType: String) =
            "${idt(level)}${protoArgs("proto_type = \"$protoType\"")}"

        fun rustEnumStart(level: Int, data: String) = "${idt(level)}pub enum $data {"
        fun rustEnumEnd(level: Int) = "${idt(level)}}"
        fun rustEnumFieldWithType(level: Int, annotation: String?, name: String, type: String) = "" +
            (annotation?.let { "${idt(level)}$it\n" } ?: "") +
            "${idt(level)}$name($type),"

        fun rustEnumField(level: Int, name: String) = "${idt(level)}$name,"

        fun rustEnumFieldProtoVariant(level: Int, variant: String) =
            "${idt(level)}${protoArgs("variant = \"$variant\"")}"

        // one of format
        fun rustOneOfDerive(level: Int) = "${idt(level)}$ONE_OF_DERIVES"
        fun rustOneOfProtoParams(level: Int, protoType: String, fieldName: String, modPath: String) =
            "${idt(level)}${
                protoArgs(
                    "proto_type = \"$protoType\"",
                    "field_name = \"$fieldName\"",
                    "mod_path = \"$modPath\""
                )
            }"


        fun rustParameterProtoParams(
            primitive: Boolean,
            box: Boolean,
            repeated: Boolean,
            enumeration: String?,
            optional: Boolean,
            parameterName: String?,
            targetIsCustomType: Boolean
        ): String? {
            val args = mutableListOf<String>()

            if (primitive) {
                args.add("primitive")
            }
            if (targetIsCustomType) {
                args.add("custom_target")
            }
            if (box) {
                args.add("boxed")
            }
            if (repeated) {
                args.add("repeated")
            }
            if (optional) {
                args.add("optional")
            }
            enumeration?.let {
                args.add("enumeration = \"$it\"")
            }
            parameterName?.let {
                args.add("field_name = \"$parameterName\"")
            }

            return if (args.isNotEmpty()) {
                protoArgs(*args.toTypedArray())
            } else {
                null
            }

        }

        // module format
        fun rustModuleStart(level: Int, data: String) = "${idt(level)}pub mod $data {"
        fun rustModuleEnd(level: Int) = "${idt(level)}}"

        fun rustModuleName(data: String): String {
            var replaced = "${data}$MODULE_SUFFIX".camelToSnakeCase()
            // remove ::_ which is not wanted
            replaced = replaced.replace("::_", "::")
            return replaced
        }

        val BUILT_IN_TYPES = mapOf(
            // QName is not handled, as it is replaced by QualifiedName in the CustomTypesEnricher
            BuiltinTypes.XSD_QNAME.qname to RustEntry.RustStruct(
                "String",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),

            BuiltinTypes.XSD_STRING.qname to RustEntry.RustStruct(
                "String",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),
            BuiltinTypes.XSD_INTEGER.qname to RustEntry.RustStruct(
                "i64",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),
            BuiltinTypes.XSD_ULONG.qname to RustEntry.RustStruct(
                "u64",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),
            BuiltinTypes.XSD_BOOL.qname to RustEntry.RustStruct(
                "bool",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),
            BuiltinTypes.XSD_LONG.qname to RustEntry.RustStruct(
                "i64",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),
            BuiltinTypes.XSD_UINT.qname to RustEntry.RustStruct(
                "u32",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),
            BuiltinTypes.XSD_INT.qname to RustEntry.RustStruct(
                "i32",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),
            BuiltinTypes.XSD_ANY_SIMPLE_TYPE.qname to RustEntry.RustStruct(
                "String",
                builtinType = true,
                rustModule = "",
                protoType = null
            ),

            BuiltinTypes.XSD_LANGUAGE.qname to RustEntry.RustStruct(
                "ProtoLanguage",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),
            BuiltinTypes.XSD_DATE.qname to RustEntry.RustStruct(
                "ProtoDate",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),
            BuiltinTypes.XSD_DATE_TIME.qname to RustEntry.RustStruct(
                "ProtoDateTime",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),
            BuiltinTypes.XSD_G_YEAR_MONTH.qname to RustEntry.RustStruct(
                "ProtoYearMonth",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),
            BuiltinTypes.XSD_G_YEAR.qname to RustEntry.RustStruct(
                "ProtoYear",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),
            BuiltinTypes.XSD_ANY_URI.qname to RustEntry.RustStruct(
                "ProtoUri",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),
            BuiltinTypes.XSD_DECIMAL.qname to RustEntry.RustStruct(
                "ProtoDecimal",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),
            BuiltinTypes.XSD_DURATION.qname to RustEntry.RustStruct(
                "ProtoDuration",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),
            BuiltinTypes.XSD_ANY.qname to RustEntry.RustStruct(
                "ProtoAny",
                builtinType = false,
                modulePath = MAPPING_TYPES_MODULE_PATH,
                rustModule = "",
                protoType = null,
                customType = true
            ),

            )

        // map of forbidden names as keys and the prefix to add the name to resolve the conflict as value
        val FORBIDDEN_PARAMETER_NAMES = mapOf(
            // ref is a rust keyword
            "ref" to "r#ref",
            // type is a rust keyword
            "type" to "r#type"
        )

        /**
         * Generates the actual .rs file content.
         *
         * @param node to generate rust for
         * @param level the indentation level of the current node
         * @param target the target in which to write the content
         * @param isNestedCall when the call is for a nested type, i.e. a module
         */
        fun generateRust(node: BaseNode, level: Int = 0, target: PrintWriter, isNestedCall: Boolean = false) {
            val rustEntry = node.languageType[OutputLanguage.Rust] as RustEntry

            val skipNested = when (rustEntry) {
                is RustEntry.RustStruct -> rustEntry.onlyGenerateNested
                is RustEntry.RustOneOf -> rustEntry.onlyGenerateNested
                is RustEntry.RustStringEnumeration -> rustEntry.onlyGenerateNested
                else -> false
            } && !isNestedCall

            if (skipNested) {
                return
            }

            val skipType = when (rustEntry) {
                is RustEntry.RustStruct -> rustEntry.skipType
                else -> false
            }

            if (skipType) {
                node.children.forEach { generateRust(it, level = level, target) }
                return
            }

            val header: String = rustEntry.toRustEnter(level = level)
            if (header.isNotBlank()) {
                // only write non-empty lines, duh
                target.println(header)
            }

            // only apply a + 1 offset when we didn't generate a header, i.e. this is just a parameter or something
            val mainOffset = when (header.isBlank()) {
                true -> level
                false -> level + 1
            }

            // generate all nested parameters
            node.children
                .filter { it.languageType[OutputLanguage.Rust] is RustEntry.RustParameter || it.languageType[OutputLanguage.Rust] is RustEntry.RustOneOfParameter }
                .forEach { generateRust(it, level = mainOffset, target) }

            val main = rustEntry.toRustMain(level = mainOffset)
            if (main.isNotBlank()) {
                target.println(main)
            }

            val footer = rustEntry.toRustExit(level = level)
            if (footer.isNotEmpty()) {
                target.println(footer)
            }

            // generate nested types
            node.children
                .filter {
                    it.languageType[OutputLanguage.Rust] !is RustEntry.RustParameter
                        && it.languageType[OutputLanguage.Rust] !is RustEntry.RustOneOfParameter
                }
                .forEach { generateRust(it, level = level, target) }


        }

        fun typeName(rustType: RustEntry, nestedLevel: Int = 0, rustModule: String): String {

            // is builtin
            val isBuiltin = when (rustType) {
                is RustEntry.RustStruct -> rustType.builtinType
                else -> false
            }

            val outnestSuper = when (isBuiltin) {
                false -> SUPER.repeat(nestedLevel)
                true -> ""
            }

            fun getRelevantModuleChunk(mod: String, rustModule: String): String? {
                if (isBuiltin) {
                    return "$mod$MOD_SEP"
                }

                val moduleProcessed = when {
                    // types which share the same path can have the prefix removed
                    mod.startsWith(rustModule) -> mod.removePrefix(rustModule).removePrefix(MOD_SEP)
                    // nested type with reference to root type must use super, blank it
                    rustModule.startsWith(mod) -> ""
                    else -> mod
                }

                return when (moduleProcessed.isNotBlank() && !moduleProcessed.endsWith(MOD_SEP)) {
                    true -> "$moduleProcessed$MOD_SEP"
                    false -> when (moduleProcessed.isBlank()) {
                        true -> null
                        false -> moduleProcessed
                    }
                }
            }

            return when (rustType) {
                is RustEntry.RustStruct -> (rustType.modulePath?.let { getRelevantModuleChunk(it, rustModule) }
                    ?: outnestSuper) + rustType.typeName

                is RustEntry.RustStringEnumeration -> (rustType.modulePath?.let {
                    getRelevantModuleChunk(
                        it,
                        rustModule
                    )
                } ?: outnestSuper) + rustType.enumName

                else -> throw Exception("Cannot handle $rustType in typeName")
            }
        }


        private fun stripModulePrefix(modulePath: String?, rustModule: String): String? {
            return modulePath?.removePrefix(rustModule)?.removePrefix(MOD_SEP)
        }


        fun toParameterName(string: String) = string.camelToSnakeCase()
        fun toEnumParameterName(string: String) =
            string.replaceFirstChar { if (it.isLowerCase()) it.titlecase() else it.toString() }

        private fun isRecursiveNode(targetNode: BaseNode, currentNode: BaseNode): Boolean {
            val visited = HashSet<BaseNode>()
            return isRecursiveNode(targetNode, currentNode, visited)
        }

        private fun isRecursiveNode(
            targetNode: BaseNode,
            currentNode: BaseNode,
            visited: MutableSet<BaseNode>
        ): Boolean {
            if (currentNode == targetNode) {
                return true
            }

            if (visited.contains(currentNode)) {
                return false
            }

            visited.add(currentNode)

            return when (val nodeType = currentNode.nodeType!!) {
                is NodeType.Message -> currentNode.children.any { isRecursiveNode(targetNode, it, visited) }
                is NodeType.OneOf -> currentNode.children.any { isRecursiveNode(targetNode, it, visited) }
                is NodeType.Parameter -> nodeType.parameterType.parent?.let { isRecursiveNode(targetNode, it, visited) }
                    ?: false

                is NodeType.BuiltinType -> false
                is NodeType.Root -> false
                is NodeType.StringEnumeration -> false
            }
        }

        fun parseConfig(config: String): RustConfig {
            return Toml.partiallyDecodeFromString(serializer(), config, TOML_TABLE_NAME)
        }
    }

    private fun determineEnumParameterName(node: BaseNode, parameter: NodeType.Parameter): String {
        // first letter must be lowercase
        val name = toEnumParameterName(
            when {
                parameter.wasAttribute -> "${node.nodeName}${converterConfig.attributeSuffix}"
                else -> node.nodeName
            }
        )

        // swap forbidden names
        return FORBIDDEN_PARAMETER_NAMES[name.lowercase()] ?: name
    }

    private fun determineParameterName(node: BaseNode, parameter: NodeType.Parameter): String {
        // first letter must be lowercase
        val name = toParameterName(
            when {
                parameter.wasAttribute -> "${node.nodeName}${converterConfig.attributeSuffix}"
                else -> node.nodeName
            }
        )

        // swap forbidden names
        return FORBIDDEN_PARAMETER_NAMES[name.lowercase()] ?: name
    }


    init {
        // attach builtin types to their nodes
        BUILT_IN_TYPES.forEach { (typeQName, rustStruct) ->
            val node = tree.findQName(typeQName)!!
            node.languageType[OutputLanguage.Rust] = rustStruct
            logger.info("Attaching builtin type $typeQName to rust type ${rustStruct.typeName}")
        }
    }

    private fun getModulePath(parentNode: BaseNode?): String? {
        // is nested when parent present and parent is a Struct too
        val parentModule = parentNode?.let { it ->
            when (val parentType = it.languageType[OutputLanguage.Rust]) {
                is RustEntry.RustStruct -> {
                    val prefix = parentType.modulePath?.let {
                        "${it}$MOD_SEP"
                    } ?: ""
                    // struct was nested too, append
                    "$prefix${rustModuleName(parentType.typeName)}"
                }

                else -> ""
            }
        }

        return parentModule
    }

    private fun processNode(node: BaseNode, parentNode: BaseNode?) {
        logger.info("Processing $node")

        node.clusteredTypes?.let { cluster ->
            logger.info("Received clustered type ${node.nodeName}, bulk handling ${cluster.joinToString { it.nodeName }} as well")
            logger.info("Cluster node types are ${cluster.joinToString { it.nodeType!!::class.simpleName!! }}")

            if (node.clusterHandled[OutputLanguage.Rust] == true) {
                logger.info("Cluster with ${node.nodeName} is already handled, skipping")
                return@let
            }

            // generate all message language types so the references to not die. also enforce clustering in one file
            // order matters, this node is now enforcing naming rules, which is why it must be handled first
            val totalCluster = listOf(node) + cluster
            totalCluster.forEach { clusterNode ->
                when (val nodeType = clusterNode.nodeType) {
                    is NodeType.Message -> {
                        logger.info("Processing cluster node $clusterNode")

                        // is nested when parent present and parent is a Struct too
                        val parentModule = getModulePath(parentNode)
                        val protoTypePath = rustBase.getFullyQualifiedProtoRustTypeName(clusterNode)

                        val languageType = RustEntry.RustStruct(
                            typeName = getEpisodeTypeName(nodeType) ?: clusterNode.nodeName,
                            _typeNameRaw = clusterNode.nodeName,
                            modulePath = getEpisodeModulePath(nodeType) ?: parentModule ?: config.rustModule,
                            rustModule = config.rustModule,
                            protoType = protoTypePath,
                        )
                        clusterNode.languageType[OutputLanguage.Rust] = languageType
                    }

                    else -> throw Exception(
                        "Can only cluster nodes which are messages, not $nodeType." +
                            " How would that even work for anything else?"
                    )
                }
            }

            // mark cluster as resolved
            totalCluster.forEach { node -> node.clusterHandled[OutputLanguage.Rust] = true }

            // no return or modified branching, we've handled the cluster and normal processing can continue
        }

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {
                logger.debug("Processing message $node")
                if (node.languageType[OutputLanguage.Rust] != null) {
                    logger.info("Node ${node.nodeName} already has Rust language type attached, skipped")
                } else {
                    // is nested when parent present and parent is a Struct too
                    val parentModule = getModulePath(parentNode)
                    val protoTypePath = rustBase.getFullyQualifiedProtoRustTypeName(node)

                    // derived from string or list types have a minLength
                    val shouldDefault = minLengthApplicable(node)
                        && (findMinLength(node)?.let { it == 0 } ?: true)


                    node.languageType[OutputLanguage.Rust] = RustEntry.RustStruct(
                        typeName = getEpisodeTypeName(nodeType) ?: node.nodeName,
                        _typeNameRaw = node.nodeName,
                        modulePath = getEpisodeModulePath(nodeType) ?: parentModule ?: config.rustModule,
                        rustModule = config.rustModule,
                        protoType = protoTypePath,
                        hasOptionalContent = shouldDefault,
                    )
                }
            }

            is NodeType.Parameter -> {
                logger.debug("Processing parameter ${nodeType.parentNode?.nodeName}")
                when (val parentLanguageType = parentNode?.let { it.languageType[OutputLanguage.Rust] }) {
                    is RustEntry.RustOneOf -> {
                        val parameterName = determineEnumParameterName(node, nodeType)
                        val parameterType =
                            nodeType.parameterType.parent!!.languageType[OutputLanguage.Rust] as RustEntry.RustStruct

                        // determine if this parameterNode references the OneOf again
                        val box = isRecursiveNode(parentNode, node)
                        val isPrimitiveProto = rustBase.isPrimitiveProtoType(nodeType)
                        val targetIsCustomType = rustBase.isCustomTypeRust(nodeType)


                        node.languageType[OutputLanguage.Rust] = RustEntry.RustOneOfParameter(
                            rustType = parameterType,
                            parameterName = parameterName,
                            box = box,
                            rustModule = config.rustModule,
                            primitive = isPrimitiveProto,
                            targetIsCustomType = targetIsCustomType,
                        )
                    }

                    is RustEntry.RustStruct, is RustEntry.RustStringEnumeration -> {
                        val parameterName = determineParameterName(node, nodeType)
                        // determine the nesting level of the parameter
                        val parameterNestingLevel = when (parentLanguageType) {
                            is RustEntry.RustStruct -> stripModulePrefix(
                                parentLanguageType.modulePath,
                                config.rustModule
                            )
                                ?.ifBlank { null }?.split("::")?.count() ?: 0

                            else -> 0
                        }

                        when (val parameterType = nodeType.parameterType.parent!!.languageType[OutputLanguage.Rust]) {
                            is RustEntry.RustStringEnumeration -> {

                                val protoEnumType =
                                    rustBase.getFullyQualifiedProtoRustTypeName(nodeType.parameterType.parent!!)

                                node.languageType[OutputLanguage.Rust] = RustEntry.RustParameter(
                                    rustType = parameterType,
                                    parentType = parentLanguageType as RustEntry,
                                    parameterName = parameterName,
                                    list = nodeType.list,
                                    optional = nodeType.optional,
                                    nestingLevel = parameterNestingLevel,
                                    rustModule = config.rustModule,
                                    primitive = false,
                                    enumeration = protoEnumType,
                                    protoParameterName = null,
                                    targetIsCustomType = false,
                                )
                            }

                            is RustEntry.RustStruct -> {
                                val box = isRecursiveNode(parentNode, node)

                                val isPrimitiveProto = rustBase.isPrimitiveProtoType(nodeType)
                                val targetIsCustomType = rustBase.isCustomTypeRust(nodeType)
                                val protoParameterName = rustBase.getProtoParameterName(node)

                                val matchingFieldName = protoParameterName == parameterName ||
                                    protoParameterName == parameterName.removePrefix("r#")

                                node.languageType[OutputLanguage.Rust] = RustEntry.RustParameter(
                                    rustType = parameterType,
                                    parentType = parentLanguageType as RustEntry,
                                    parameterName = parameterName,
                                    list = nodeType.list,
                                    optional = nodeType.optional,
                                    nestingLevel = parameterNestingLevel,
                                    box = box,
                                    rustModule = config.rustModule,
                                    primitive = isPrimitiveProto,
                                    targetIsCustomType = targetIsCustomType,
                                    enumeration = null,
                                    protoParameterName = if (!matchingFieldName) protoParameterName else null
                                )
                            }

                            else -> {
                                throw Exception("Unsupported language type $parameterType")
                            }
                        }
                    }
                }
            }

            is NodeType.StringEnumeration -> {
                // is nested when parent present and parent is a Struct too
                val parentModule = getModulePath(parentNode)

                val protoTypePath = rustBase.getFullyQualifiedProtoRustTypeName(node)

                node.languageType[OutputLanguage.Rust] = RustEntry.RustStringEnumeration(
                    enumName = "EnumType",
                    enumValues = nodeType.values.toList(),
                    modulePath = parentModule,
                    rustModule = config.rustModule,
                    protoType = protoTypePath,
                )
            }

            is NodeType.OneOf -> {
                val protoTypePath = rustBase.getFullyQualifiedProtoRustTypeName(parentNode!!)

                node.languageType[OutputLanguage.Rust] = RustEntry.RustOneOf(
                    oneOfName = nodeType.parent!!.nodeName,
                    rustModule = config.rustModule,
                    // TODO LDe: This is entirely guesswork right now
                    protoType = protoTypePath,
                    protoFieldName = nodeType.parent!!.nodeName.camelToSnakeCase(),
                    // TODO: LDe properly fix this underscore stuff
                    protoModPath = protoTypePath.camelToSnakeCase()
                        .replace("::_", "::") + MOD_SEP + nodeType.parent!!.nodeName
                )
            }
            // pass
            is NodeType.BuiltinType -> {
                // nothing to do
            }

            else -> {
                throw Exception("Handle unknown node type ${node.nodeType}")
            }
        }

        node.children.forEach { processNode(it, node) }
    }

    private fun restructureNesting(node: BaseNode) {
        logger.debug { "restructureNesting $node" }
        node.children.forEach {
            // fix nesting in children first, before we change things around
            restructureNesting(it)
        }

        when (val languageType = node.languageType[OutputLanguage.Rust]) {
            is RustEntry.RustStruct -> {
                // determine if this is a OneOf container
                val isOneOfContainer = node.children.none {
                    it.languageType[OutputLanguage.Rust] !is RustEntry.RustOneOf
                } && node.children.count {
                    it.languageType[OutputLanguage.Rust] is RustEntry.RustOneOf
                } == 1

                val isUnderRoot = tree.children.contains(node)

                if (isOneOfContainer) {
                    // tag DataClass as skip so we do not generate it
                    languageType.onlyGenerateNested = !isUnderRoot // only when not directly below root do we unnest
                    languageType.skipType = true
                } else {
                    // take out any messages in the children and attach them to the DataClass
                    val nestedTypes = node.children.filter {
                        when (it.languageType[OutputLanguage.Rust]) {
                            is RustEntry.RustStruct -> {
                                true
                            }

                            is RustEntry.RustStringEnumeration, is RustEntry.RustOneOf -> true
                            else -> false
                        }
                    }.toList()

                    if (nestedTypes.isNotEmpty()) {
                        logger.info { "Restructuring nesting for $node, moving children $nestedTypes" }
                        nestedTypes.map { it.languageType[OutputLanguage.Rust]!! }
                            .forEach {
                                when (it) {
                                    is RustEntry.RustStruct -> it.onlyGenerateNested = true
                                    is RustEntry.RustStringEnumeration -> it.onlyGenerateNested = true
                                    is RustEntry.RustOneOf -> it.onlyGenerateNested = true
                                }
                            }
                        languageType.nestedTypes.addAll(nestedTypes)
                    }

                }
            }

            else -> {
                // nothing to do
            }
        }
    }

    override fun run() {

        val outputFolder = File(converterConfig.outputFolder, config.outputFolder)
        requireLegalFolderContents(outputFolder, RUST_FILE_EXTENSION)
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()

        val outputFile = File(outputFolder, config.outputFile)

        // generate types for every "root" type, i.e. the one on depth 1
        // every other type is nested within those and will be covered by the generators
        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node ->
            processNode(node, null)
        }

        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node ->
            restructureNesting(node)
        }

        tree.children
            .filter { it.nodeType !is NodeType.BuiltinType }
            .filter {
                when (val languageType = it.languageType[OutputLanguage.Rust]) {
                    is RustEntry.RustStruct -> {
                        // filter out empty structs
                        val filter = languageType.nestedTypes.isNotEmpty() || it.children.isNotEmpty()
                        if (!filter) {
                            logger.warn { "Filtering out empty data class ${it.nodeName}" }
                        }
                        filter
                    }

                    else -> true
                }
            }
            .filter {
                when (val nodeType = it.nodeType) {
                    is NodeType.Message -> !nodeType.skipGenerating
                    else -> true
                }
            }
            .filter {
                when (val nodeType = it.nodeType) {
                    is NodeType.Message -> !nodeType.customType || generateCustomTypes
                    else -> true
                }
            }
            .forEach { node ->
                // determine file for this message
                check(node.languageType[OutputLanguage.Rust] is RustEntry.RustStruct) { "Expected RustStruct, was ${node.languageType[OutputLanguage.Rust]}" }

                val os = ByteArrayOutputStream()
                PrintWriter(os).use { printWriter ->
                    generateRust(node, target = printWriter)
                }

                os.toString().also {
                    logger.debug { "--\n$it" }
                    outputFile.appendText(it)
                }
            }
    }

    override fun episodeData(node: BaseNode): EpisodeData {
        return when (val languageType = node.languageType[OutputLanguage.Rust]!!) {
            is RustEntry.RustStruct -> EpisodeData.Model(
                languageType.modulePath ?: "",
                languageType.typeName
            )

            else -> TODO("handle $languageType")
        }
    }


    private fun getEpisodeTypeName(it: NodeType.Message): String? {
        return it.episodePluginData[pluginIdentifier()]?.let { return@let (it as EpisodeData.Model).typeName }
    }

    private fun getEpisodeModulePath(it: NodeType.Message): String? {
        return it.episodePluginData[pluginIdentifier()]
            ?.let {
                return@let (it as EpisodeData.Model).packageName
                    .replacePrefixWithCrate(config.rustModule.split(MOD_SEP).first())
            }
    }

    override fun pluginIdentifier(): String {
        @Suppress("SpellCheckingInspection")
        return "rustpluginv1"
    }
}