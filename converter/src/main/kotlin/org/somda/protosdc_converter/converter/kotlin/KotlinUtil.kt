package org.somda.protosdc_converter.converter.kotlin

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.requireLegalFolderContents
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.nio.file.Paths

const val KOTLIN_MODEL_PLUGIN_IDENTIFIER = "kotlinmodelpluginv1"
const val KOTLIN_FILE_EXTENSION = "kt"
const val KOTLIN_TOML_TABLE_NAME = "Kotlin"

/**
 * Represents an output folder that is supposed to be either include package hierarchies or not.
 *
 * @param folder the output folder.
 * @param createHierarchy true if package hierarchies are included, false otherwise.
 */
data class OutputFolder(val folder: File, val createHierarchy: Boolean)

/**
 * Determines the name of a kotlin type, i.e. data class or enumeration.
 */
fun KotlinEntry.typeName(): String {
    return when (this) {
        is KotlinEntry.DataClass -> this.typeName
        is KotlinEntry.KotlinStringEnumeration -> this.enumName
        else -> throw Exception("$this does not have a typeName")
    }
}

/**
 * Determines the full file name for a type, including .kt suffix.
 *
 * @return full file name
 */
fun KotlinEntry.DataClass.fileNameFromType(): String {
    return this.fileName ?: appendKotlinFileExtension(this.typeNameRaw)
}

/**
 * Determines the package name of a type known from an episode file.
 */
fun NodeType.Message.kotlinEpisodePackageName(): String? {
    return kotlinEpisodePluginData()?.packageName
}

/**
 * Determines the type name of a type known from an episode file.
 */
fun NodeType.Message.kotlinEpisodeTypeName(): String? {
    return kotlinEpisodePluginData()?.typeName
}

/**
 * Gets the episode data for the Kotlin plugin.
 */
fun NodeType.Message.kotlinEpisodePluginData(): EpisodeData.Model? {
    return episodePluginData[KOTLIN_MODEL_PLUGIN_IDENTIFIER] as? EpisodeData.Model
}

/**
 * Gets the language-specific type information for the Kotlin target language.
 *
 * @return a [KotlinEntry] if existing or null otherwise.
 */
fun BaseNode.kotlinLanguageTypeOrNull(): KotlinEntry? {
    return this.languageType[OutputLanguage.Kotlin] as? KotlinEntry
}

/**
 * Gets the language-specific type information for the Kotlin target language.
 *
 * @return a [KotlinEntry] if existing or throws if none exists.
 */
fun BaseNode.kotlinLanguageType(): KotlinEntry {
    return kotlinLanguageTypeOrNull() as KotlinEntry
}

/**
 * Sets the language-specific type information for the Kotlin target language.
 */
fun BaseNode.kotlinLanguageType(languageType: BaseLanguageType) {
    this.languageType[OutputLanguage.Kotlin] = languageType
}

/**
 * Marks a cluster as handles for the Kotlin target language.
 */
fun BaseNode.kotlinSetClusterHandled() {
    clusterHandled[OutputLanguage.Kotlin] = true
}

/**
 * Checks if a cluster was handled already for the Kotlin target language.
 */
fun BaseNode.kotlinClusterHandled() = clusterHandled[OutputLanguage.Kotlin] ?: false

/**
 * Checks if for the Kotlin target language the resulting data class of this base node is empty.
 *
 * Empty means that neither nested types nor children exist.
 */
fun BaseNode.kotlinIsEmptyDataClass() = when (val languageType = this.kotlinLanguageType()) {
    is KotlinEntry.DataClass -> languageType.nestedTypes.isEmpty() && this.children.isEmpty()
    else -> true
}

/**
 * Inspects the `skipGenerating` flag of a [NodeType.Message].
 *
 * @return true if this node is a [NodeType.Message] and the `skipGenerating` is true, false otherwise.
 */
fun NodeType.skipGenerating() = when (this) {
    is NodeType.Message -> this.skipGenerating
    else -> false
}

/**
 * Inspects the `customType` flag of a [NodeType.Message].
 *
 * @return true if this node is a [NodeType.Message] and the `customType` is true, false otherwise.
 */
fun NodeType.isCustomType() = when (this) {
    is NodeType.Message -> this.customType
    else -> false
}

/**
 * Recursively removes Kotlin source files  and folders from the given folders.
 *
 * Aborts if non-Kotlin files are detected.
 */
fun cleanUpOutputFolders(
    baseDir: String,
    foldersToCleanup: List<String>
) {
    return foldersToCleanup.forEach { folderName ->
        File(baseDir, folderName).let { folder ->
            requireLegalFolderContents(folder, KOTLIN_FILE_EXTENSION)
            folder.deleteRecursively()
        }
    }
}

/**
 * Unifies folder lists to a single list.
 *
 * @param baseDir path to the directory that contains output folders.
 * @param outputFoldersHierarchy output folders to which package directories shall be added.
 * @param outputFoldersNoHierarchy output folders to which no package directories shall be added.
 */
fun setupOutputFolders(
    baseDir: String,
    outputFoldersHierarchy: List<String>,
    outputFoldersNoHierarchy: List<String>,
    noneFoundErrorMessage: () -> String
): List<OutputFolder> {
    val mappedFolderNames = outputFoldersHierarchy.map { it to true } +
            outputFoldersNoHierarchy.map { it to false }
    return mappedFolderNames.map { (folderName, createHierarchy) ->
        File(baseDir, folderName).let { folder ->
            folder.mkdirs()
            OutputFolder(folder, createHierarchy)
        }
    }.toList().also {
        check(it.isNotEmpty()) {
            noneFoundErrorMessage()
        }
    }
}

/**
 * Creates file objects for all output folders based on a Kotlin package and Kotlin file name.
 *
 * Example:
 *
 * - outputFolders: folder1, folder 2
 * - kotlinPackage: org.example
 * - fileName: Foobar.kt
 *
 * Resulting list:
 *
 * - folder1/org/example/Foobar.kt
 * - folder2/org/example/Foobar.kt
 *
 * Folders are created when non-existing.
 */
fun determineFilesToWrite(
    outputFolders: List<OutputFolder>,
    kotlinPackage: String,
    fileName: String
): List<File> {
    return outputFolders.map { outputFolder ->
        val targetDir = if (outputFolder.createHierarchy) {
            val segments = kotlinPackage.split(".").toTypedArray()
            File(Paths.get(outputFolder.folder.absolutePath, *segments).toUri()).also {
                if (!it.exists()) {
                    it.mkdirs()
                }
            }.absoluteFile
        } else {
            outputFolder.folder.absoluteFile
        }
        File(targetDir, fileName)
    }
}

fun appendKotlinFileExtension(name: String) = "$name.$KOTLIN_FILE_EXTENSION"

private object FileWriter : Logging

fun writeByteStreamToFile(byteStream: ByteArrayOutputStream, filesToWrite: List<File>) {
    byteStream.toString().also { content ->
        FileWriter.logger.debug { "--\n$content" }
        filesToWrite.forEach {
            FileWriter.logger.info { "Writing to ${it.absolutePath}" }
            it.appendText(content)
        }
    }
}