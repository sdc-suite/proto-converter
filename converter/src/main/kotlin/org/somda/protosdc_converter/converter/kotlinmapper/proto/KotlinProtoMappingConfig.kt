package org.somda.protosdc_converter.converter.kotlinmapper.proto

@kotlinx.serialization.Serializable
data class KotlinProtoMappingConfig(
    val mappingPackage: String,
    val outputFolders: List<String> = emptyList(),
    val outputFoldersNoHierarchy: List<String> = emptyList(),
    val kotlinToProtoClassName: String = "KotlinToProto",
    val protoToKotlinClassName: String = "ProtoToKotlin"
)