package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.QuickXmlParserConfig
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.parserErrorElementError
import org.somda.protosdc_converter.converter.rust.rustToStringCall
import org.somda.protosdc_converter.converter.rust.simpleParserSignature
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

data class StringEnumParser(val parserName: String, val dataType: BaseNode, val rustModulePath: String) :
    ParserTracker {
    // TODO: This only supports enum values which are within ascii, as for performance reasons no decoding is taking place
    override fun generateParser(
        config: QuickXmlParserConfig,
        primitiveParsers: MutableMap<BaseNode, RustParser>,
        simpleParsers: MutableMap<BaseNode, RustParser>,
        complexParsers: MutableMap<BaseNode, RustParser>
    ): String {
        check(dataType.children.size == 2)

        val dataTypeRustStruct = dataType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct

        val stringEnum = dataType.children[0]
        val nodeTypeEnumeration = stringEnum.nodeType!! as NodeType.StringEnumeration
        val rustStringEnum = stringEnum.languageType[OutputLanguage.Rust]!! as RustEntry.RustStringEnumeration

        val fullyQualifiedRustTypeName = dataTypeRustStruct.fullyQualifiedName(config.modelRustModuleSubstitute)
        val fullyQualifiedRustEnumTypeName = rustStringEnum.fullyQualifiedName(config.modelRustModuleSubstitute)

        val targetField = dataType.children[1]
        val rustTargetField = targetField.languageType[OutputLanguage.Rust]!! as RustEntry.RustParameter

        val start = """
${simpleParserSignature(fullyQualifiedRustTypeName, config.extensionAssociatedType)}
        Ok($fullyQualifiedRustTypeName {
            ${rustTargetField.parameterName}: match data {
"""

        val payload = nodeTypeEnumeration.values.zip(rustStringEnum.enumValues).map { (xml, rust) ->
            "               b\"$xml\" => Ok($fullyQualifiedRustEnumTypeName::$rust),\n"
        }.joinToString(separator = "")
        val end = """                _ => Err(${parserErrorElementError(rustToStringCall(dataType.nodeName))}),
            }?
        })
    }
}
            """

        return start + payload + end
    }
}