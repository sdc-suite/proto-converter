package org.somda.protosdc_converter.converter.rust

import com.akuleshov7.ktoml.Toml
import kotlinx.serialization.serializer
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.BaseNodePlugin
import org.somda.protosdc_converter.converter.ConverterConfig
import org.somda.protosdc_converter.converter.EXTENSION_NODE_NAME
import org.somda.protosdc_converter.converter.PARTICIPANT_MODEL_NAMESPACE
import org.somda.protosdc_converter.converter.QUALIFIED_NAME_NODE_NAME
import org.somda.protosdc_converter.converter.requireLegalFolderContents
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.MOD_SEP
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.xml.WriterAttributeInfo
import org.somda.protosdc_converter.converter.rust.xml.ElementWriter
import org.somda.protosdc_converter.converter.rust.xml.OneOfContentWriter
import org.somda.protosdc_converter.converter.rust.xml.RustWriter
import org.somda.protosdc_converter.converter.rust.xml.SimpleTypeWriter
import org.somda.protosdc_converter.converter.rust.xml.StringEnumWriter
import org.somda.protosdc_converter.converter.rust.xml.WriterElementInfo
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintWriter
import javax.xml.namespace.QName


const val WRITER_IMPORTS = """use quick_xml::events::attributes::Attribute;
use protosdc_xml::{SimpleXmlTypeWrite, ComplexXmlTypeWrite};
"""

const val WRITER_DEFINITIONS = """
type QName = (Option<&'static str>, &'static str);
"""

const val WRITER_ERROR = "protosdc_xml::WriterError"

///
/// Constants and utilities for quick xml
///
const val PROTOSDC_WRITER_TYPE = "protosdc_xml::XmlWriter<Vec<u8>>"

const val WRITER = "writer"
const val DATA = "data"
const val SELF = "self"
const val XSI_TYPE_ARG = "xsi_type"
const val UNIT = "()"

fun primitiveWriterTemplate(name: String) = """${primitiveWriterSignature(name, "String")}
    Ok($DATA.clone().into_bytes())
}
"""


const val EXTENSION_WRITER_NAME = "write_extension_todo_later"
fun redirectExtensionWriter(modulePath: String, call: String) = """
${elementWriterSignature("$modulePath::Extension")}
        $call(self, $TAG_NAME, $WRITER, $XSI_TYPE_ARG)
    }
}
"""

fun extensionWriter(modulePath: String) = """
${elementWriterSignature("$modulePath::Extension")}
        $WRITER
            .write_start(${some("\"http://standards.ieee.org/downloads/11073/11073-10207-2017/extension\"")}, "Extension")?
            .write_end(${some("\"http://standards.ieee.org/downloads/11073/11073-10207-2017/extension\"")}, "Extension")?;
        Ok(())
    }
}
"""

const val PRIMITIVE_TYPE_STRING_WRITER_NAME = "write_xsd_string"
val PRIMITIVE_TYPE_STRING_WRITER_QNAME = BuiltinTypes.XSD_STRING.qname
val PRIMITIVE_TYPE_STRING_WRITER = primitiveWriterTemplate(PRIMITIVE_TYPE_STRING_WRITER_NAME)

const val PRIMITIVE_TYPE_DECIMAL_WRITER_NAME = "write_xsd_decimal"
val PRIMITIVE_TYPE_DECIMAL_WRITER_QNAME = BuiltinTypes.XSD_DECIMAL.qname
val PRIMITIVE_TYPE_DECIMAL_WRITER =
    """${primitiveWriterSignature(PRIMITIVE_TYPE_DECIMAL_WRITER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoDecimal")}
    let response: String = (*data).clone().to_string();
    Ok(response.into_bytes())
}"""

const val PRIMITIVE_TYPE_ANY_URI_WRITER_NAME = "write_xsd_any_uri"
val PRIMITIVE_TYPE_ANY_URI_WRITER_QNAME = BuiltinTypes.XSD_ANY_URI.qname
val PRIMITIVE_TYPE_ANY_URI_WRITER =
    """${primitiveWriterSignature(PRIMITIVE_TYPE_ANY_URI_WRITER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoUri")}
    let response: String = data.clone().into();
    Ok(response.into_bytes())
}"""

const val PRIMITIVE_TYPE_LANGUAGE_WRITER_NAME = "write_xsd_language"
val PRIMITIVE_TYPE_LANGUAGE_WRITER_QNAME = BuiltinTypes.XSD_LANGUAGE.qname
val PRIMITIVE_TYPE_LANGUAGE_WRITER =
    """${primitiveWriterSignature(PRIMITIVE_TYPE_LANGUAGE_WRITER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoLanguage")}
    let response: String = data.clone().into();
    Ok(response.into_bytes())
}"""

const val PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_WRITER_NAME = "write_xsd_any_simple_type"
val PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_WRITER_QNAME = BuiltinTypes.XSD_ANY_SIMPLE_TYPE.qname
val PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_WRITER = primitiveWriterTemplate(PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_WRITER_NAME)

const val PRIMITIVE_TYPE_DURATION_WRITER_NAME = "write_xsd_duration"
val PRIMITIVE_TYPE_DURATION_WRITER_QNAME = BuiltinTypes.XSD_DURATION.qname
val PRIMITIVE_TYPE_DURATION_WRITER =
    """${primitiveWriterSignature(PRIMITIVE_TYPE_DURATION_WRITER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoDuration")}
    let response: String = chrono::Duration::from_std(**data).expect("what").to_string();
    Ok(response.into_bytes())
}"""


const val PRIMITIVE_TYPE_BOOL_WRITER_NAME = "write_xsd_bool"
val PRIMITIVE_TYPE_BOOL_WRITER_QNAME = BuiltinTypes.XSD_BOOL.qname
val PRIMITIVE_TYPE_BOOL_WRITER = """${primitiveWriterSignature(PRIMITIVE_TYPE_BOOL_WRITER_NAME, "bool")}
    Ok(match $DATA {
        true => b"true".to_vec(),
        false => b"false".to_vec(),
    })
}
"""

const val PRIMITIVE_TYPE_QUALIFIED_NAME_WRITER_NAME = "write_qualified_name"
fun simpleTypeQualifiedNameWriter(modulePath: String) = """${simpleWriterSignature("$modulePath::QualifiedName")}
        let tag_to_write = writer.qualify_tag_add_ns(Some(&self.namespace), &self.local_name)?;
        Ok(tag_to_write.into_bytes())
    }
}
"""


// generate writers for all integer-style target types we're using
val WRITER_INTEGER_TYPES_MAP: List<Triple<String, QName, String>> = RustGenerator.BUILT_IN_TYPES
    .filter { it.value.typeName in listOf("i32", "i64", "u32", "u64") }
    .map {
        val parserName = "write_xsd_" + it.key.localPart.camelToSnakeCase()

        val parserCode = """${primitiveWriterSignature(parserName, it.value.typeName)}
    Ok($DATA.to_string().into_bytes())
}"""
        Triple(parserName, it.key, parserCode)
    }.toList()


const val DATE_OF_BIRTH_UNION_WRITER_NAME = "write_date_of_birth_union"
val DATE_OF_BIRTH_UNION_WRITER_PARENT_QNAME = QName(PARTICIPANT_MODEL_NAMESPACE, "DateOfBirth")
fun dateOfBirthUnionWriter(modulePath: String) = """${
    primitiveWriterSignature(
        DATE_OF_BIRTH_UNION_WRITER_NAME,
        "$modulePath::patient_demographics_core_data_mod::DateOfBirth"
    )
}
    // TODO: Correctly parse dateTime etc.
    let response: String = match $DATA {
        $modulePath::patient_demographics_core_data_mod::DateOfBirth::DateTime(y) => y.clone().into(),
        $modulePath::patient_demographics_core_data_mod::DateOfBirth::Date(y) => y.clone().into(),
        $modulePath::patient_demographics_core_data_mod::DateOfBirth::GYearMonth(y) => y.clone().into(),
        $modulePath::patient_demographics_core_data_mod::DateOfBirth::GYear(y) => y.clone().into(),
    };
    Ok(response.into_bytes())
}"""

const val DATE_TIME_WRITER_NAME = "write_date_time"
val DATE_TIME_WRITER_QNAME = BuiltinTypes.XSD_DATE_TIME.qname
val DATE_TIME_WRITER =
    """${primitiveWriterSignature(DATE_TIME_WRITER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoDateTime")}
    let response: String = data.clone().into();
    Ok(response.into_bytes())
}"""

const val DATE_WRITER_NAME = "write_date"
val DATE_WRITER_QNAME = BuiltinTypes.XSD_DATE.qname
val DATE_WRITER = """${primitiveWriterSignature(DATE_WRITER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoDate")}
    let response: String = data.clone().into();
    Ok(response.into_bytes())
}"""

const val YEAR_MONTH_WRITER_NAME = "write_year_month"
val YEAR_MONTH_WRITER_QNAME = BuiltinTypes.XSD_G_YEAR_MONTH.qname
val YEAR_MONTH_WRITER =
    """${primitiveWriterSignature(YEAR_MONTH_WRITER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoYearMonth")}
    let response: String = data.clone().into();
    Ok(response.into_bytes())
}"""

const val YEAR_WRITER_NAME = "write_year"
val YEAR_WRITER_QNAME = BuiltinTypes.XSD_G_YEAR.qname
val YEAR_WRITER = """${primitiveWriterSignature(YEAR_WRITER_NAME, "$MAPPING_TYPES_MODULE_PATH::ProtoYear")}
    let response: String = data.clone().into();
    Ok(response.into_bytes())
}"""


private fun primitiveWriterSignature(writerName: String, dataType: String) =
    "${RustQuickXmlParser.allowUnusedAssignment()}\npub fn ${writerName}($DATA: &$dataType, $WRITER: &mut $PROTOSDC_WRITER_TYPE) -> Result<Vec<u8>, $WRITER_ERROR> {"

internal fun simpleWriterSignature(dataType: String) =
    """${RustQuickXmlParser.allowUnusedAssignment()}
impl protosdc_xml::SimpleXmlTypeWrite for $dataType {
    fn to_xml_simple(&self, $WRITER: &mut $PROTOSDC_WRITER_TYPE) -> Result<Vec<u8>, $WRITER_ERROR> {
    """

internal fun elementWriterSignature(dataType: String) =
    """${RustQuickXmlParser.allowUnusedAssignment()}
impl protosdc_xml::ComplexXmlTypeWrite for $dataType {
    fn to_xml_complex(&self, $TAG_NAME: Option<QName>, $WRITER: &mut $PROTOSDC_WRITER_TYPE, $XSI_TYPE_ARG: bool) -> Result<(), $WRITER_ERROR> {
    """

internal fun callPrimitiveWriter(config: QuickXmlWriterConfig, writer: RustWriter, value: String) =
    "${writer.fullyQualifiedName(config.modelRustModuleSubstitute)}($value, $WRITER)"

internal fun callSimpleWriter(variableName: String) =
    "$variableName.to_xml_simple($WRITER)"

//private fun callElementWriter(writer: RustWriter, value: String, tagName: String?, xsiType: Boolean = false) =
//    "${writer.module?.let { it + MOD_SEP } ?: ""}${writer.name}($value, ${tagName ?: "None"}, $WRITER, $xsiType)"
internal fun callElementWriter(variableName: String, tagName: String?, xsiType: Boolean = false) =
    "$variableName.to_xml_complex(${tagName ?: "None"}, $WRITER, $xsiType)"

internal fun validateName(name: String): String {
    return RustGenerator.FORBIDDEN_PARAMETER_NAMES[name] ?: name
}

internal fun some(name: String) = "Some($name)"

internal fun accessAncestor(ancestors: List<BaseNode>, ancestor: BaseNode): String? {
    return when (val ancestorIdx = ancestors.indexOf(ancestor)) {
        -1 -> null
        else -> {
            ancestors.subList(0, ancestorIdx).joinToString(separator = ".") {
                val ancestorNode = it.children
                    .filter { it.nodeType is NodeType.Parameter }
                    .find {
                        val nodeType = it.nodeType as NodeType.Parameter
                        nodeType.inheritance
                    }!!

                val dataTypeRustStruct =
                    ancestorNode.languageType[OutputLanguage.Rust]!! as RustEntry.RustParameter
                dataTypeRustStruct.parameterName
            }
        }
    }
}

private fun getParameterAccessThroughAncestors(
    ancestors: List<BaseNode>,
    ancestor: BaseNode,
    parameter: BaseNode
): String? {
    return when (val ancestorIdx = ancestors.indexOf(ancestor)) {
        -1 -> null
        else -> {
            // get path to ancestor if the index of the ancestor is not the first element
            val prefix = if (ancestorIdx > 1) {
                accessAncestor(ancestors.subList(0, ancestorIdx), ancestor)
            } else {
                null
            }

            val final = ancestors[ancestorIdx].children
                .filter { it.nodeType is NodeType.Parameter }
                .find { it == parameter }
                ?.let { node ->
                    val dataTypeRustStruct =
                        node.languageType[OutputLanguage.Rust]!! as RustEntry.RustParameter
                    dataTypeRustStruct.parameterName
                }

            final?.let { f ->
                (prefix?.let { "$it." } ?: "") + f
            }
        }
    }
}

@kotlinx.serialization.Serializable
data class QuickXmlWriterConfig(
    val rustModule: String,
    val outputFolder: String = "rust_quick_xml_writer/",
    val outputFile: String = "writer.rs",
    val extensionWriterCall: CallOverride? = null,
    val modelRustModuleSubstitute: String? = null,
) {
    init {
        require(!rustModule.contains("."))
        require(!rustModule.endsWith("::"))
        require(outputFolder.endsWith("/"))
        require(outputFile.endsWith(".$RUST_FILE_EXTENSION"))
        require(!(modelRustModuleSubstitute?.endsWith("::") ?: false))
    }
}

class RustQuickXmlWriter(
    private val tree: BaseNode,
    private val converterConfig: ConverterConfig,
    private val rustConfig: RustConfig,
    private val config: QuickXmlWriterConfig,
    private val generateCustomTypes: Boolean = true,
) : BaseNodePlugin {

    companion object : Logging {
        private const val TOML_TABLE_NAME = "QuickXmlWriter"

        fun parseConfig(config: String): QuickXmlWriterConfig {
            return Toml.partiallyDecodeFromString(serializer(), config, TOML_TABLE_NAME)
        }
    }

    private val builtinWriters: List<Triple<String, QName, String>> = listOf(
        Triple(PRIMITIVE_TYPE_STRING_WRITER_NAME, PRIMITIVE_TYPE_STRING_WRITER_QNAME, PRIMITIVE_TYPE_STRING_WRITER),
        Triple(PRIMITIVE_TYPE_DECIMAL_WRITER_NAME, PRIMITIVE_TYPE_DECIMAL_WRITER_QNAME, PRIMITIVE_TYPE_DECIMAL_WRITER),
        Triple(PRIMITIVE_TYPE_ANY_URI_WRITER_NAME, PRIMITIVE_TYPE_ANY_URI_WRITER_QNAME, PRIMITIVE_TYPE_ANY_URI_WRITER),
        Triple(
            PRIMITIVE_TYPE_DURATION_WRITER_NAME,
            PRIMITIVE_TYPE_DURATION_WRITER_QNAME,
            PRIMITIVE_TYPE_DURATION_WRITER
        ),
        Triple(
            PRIMITIVE_TYPE_LANGUAGE_WRITER_NAME,
            PRIMITIVE_TYPE_LANGUAGE_WRITER_QNAME,
            PRIMITIVE_TYPE_LANGUAGE_WRITER
        ),
        Triple(PRIMITIVE_TYPE_BOOL_WRITER_NAME, PRIMITIVE_TYPE_BOOL_WRITER_QNAME, PRIMITIVE_TYPE_BOOL_WRITER),
        Triple(
            PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_WRITER_NAME,
            PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_WRITER_QNAME,
            PRIMITIVE_TYPE_ANY_SIMPLE_TYPE_WRITER
        ),
        Triple(DATE_TIME_WRITER_NAME, DATE_TIME_WRITER_QNAME, DATE_TIME_WRITER),
        Triple(DATE_WRITER_NAME, DATE_WRITER_QNAME, DATE_WRITER),
        Triple(YEAR_MONTH_WRITER_NAME, YEAR_MONTH_WRITER_QNAME, YEAR_MONTH_WRITER),
        Triple(YEAR_WRITER_NAME, YEAR_WRITER_QNAME, YEAR_WRITER),
    ) + WRITER_INTEGER_TYPES_MAP

    // track all generated parsers by name, api is defined by being simple or complex
    private val primitiveWriters: MutableMap<BaseNode, RustWriter> = mutableMapOf()
    private val simpleWriters: MutableMap<BaseNode, RustWriter> = mutableMapOf()
    private val complexWriters: MutableMap<BaseNode, RustWriter> = mutableMapOf()

    init {
        // attach builtin parsers
        builtinWriters.forEach { (parserName, qname, _) ->
            val node = tree.findQName(qname = qname)!!
            primitiveWriters[node] = RustWriter(config.rustModule, parserName)
        }

        if (generateCustomTypes) {
            // insert qualified name parser
            val qualifiedNameNode = tree.children
                .filter { it.isCustomType }
                .find { it.nodeName == QUALIFIED_NAME_NODE_NAME }!!
            simpleWriters[qualifiedNameNode] = RustWriter(config.rustModule, PRIMITIVE_TYPE_QUALIFIED_NAME_WRITER_NAME)

            // insert union parser
            tree.findQName(DATE_OF_BIRTH_UNION_WRITER_PARENT_QNAME)?.let { dobParent ->
                val dobNode = dobParent.children[0]
                check(dobNode.nodeType is NodeType.OneOf)

                primitiveWriters[dobNode] = RustWriter(config.rustModule, DATE_OF_BIRTH_UNION_WRITER_NAME)
            } ?: run { logger.warn("Could not find Date of Birth Union to attach to") }

            // nop extension type for now
            logger.warn("NOP-ing extension type, fixme")
            val extensionTypeNode = tree.findQName(QName(Constants.EXT_NAMESPACE, "ExtensionType"))!!
            complexWriters[extensionTypeNode] =
                RustWriter(config.rustModule, "this_should_be_unused_due_to_replacement")

            val extensionNode = tree.children
                .filter { it.isCustomType }
                .find { it.nodeName == EXTENSION_NODE_NAME }!!

            complexWriters[extensionNode] = RustWriter(config.rustModule, EXTENSION_WRITER_NAME)

        }

        tree.children.forEach { child ->
            when (val nodeType = child.nodeType) {
                is NodeType.Message -> {
                    nodeType.episodePluginData[pluginIdentifier()]?.let {
                        when (val episodeData = it) {
                            is EpisodeData.Mapper -> when (episodeData.sourceToTargetFunction) {
                                "simple" -> {
                                    simpleWriters[child] = RustWriter(
                                        episodeData.sourceStructName.ifBlank { null },
                                        episodeData.sourceTypeName
                                    )
                                }

                                "complex" -> {
                                    complexWriters[child] = RustWriter(
                                        episodeData.sourceStructName.ifBlank { null },
                                        episodeData.sourceTypeName
                                    )
                                }

                                else -> {
                                    // nop
                                }
                            }

                            else -> {
                                // nop
                            }
                        }
                    }
                }

                else -> {
                    // nop
                }
            }
        }

    }




    private fun processNode(node: BaseNode, parentNode: BaseNode?, target: PrintWriter) {
        logger.info("Processing $node")

        node.clusteredTypes?.let { cluster ->
            logger.info("Received clustered type ${node.nodeName}, bulk handling ${cluster.joinToString { it.nodeName }} as well")
            logger.info("Cluster node types are ${cluster.joinToString { it.nodeType!!::class.simpleName!! }}")

            if (node.clusterHandled[OutputLanguage.RustQuickXMLWriter] == true) {
                logger.info("Cluster with ${node.nodeName} is already handled, skipping")
                return@let
            }

            val totalCluster = listOf(node) + cluster

            totalCluster.forEach { clusterNode ->
                val writerName = "write_${clusterNode.nodeName.camelToSnakeCase()}"
                // simply inject complex type parsers, let's see what happens
                complexWriters[clusterNode] = RustWriter(config.rustModule, writerName + COMPLEX_SUFFIX)
            }

            // mark cluster as resolved
            totalCluster.forEach { node -> node.clusterHandled[OutputLanguage.RustQuickXMLWriter] = true }

            // no return or modified branching, we've handled the cluster and normal processing can continue
        }

        if (node.isCustomType) {
            // TODO handle custom types
            logger.warn("Handle custom types")
            return
        }

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {
                // we do not care about messages below the root without qname, might be something custom or whatever
                if (nodeType.qname == null && parentNode == null) {
                    logger.debug("node below root without qname: $node")
                }

                val writerPrefixName = "write_"

                val writerName: String = when (parentNode) {
                    null -> "$writerPrefixName${node.nodeName.camelToSnakeCase()}"
                    else -> {
                        val parentThing = parentNode.nodeName.camelToSnakeCase()
                        when (node.languageType[OutputLanguage.Rust] is RustEntry.RustStruct) {
                            true -> {
                                val rustStruct =
                                    node.languageType[OutputLanguage.Rust] as RustEntry.RustStruct
                                val middleSegment =
                                    rustStruct.modulePath?.replace(MOD_SEP, "_") ?: parentThing
                                "$writerPrefixName${middleSegment}_${node.nodeName.camelToSnakeCase()}"
                            }

                            false -> "$writerPrefixName${parentNode.nodeName.camelToSnakeCase()}_${node.nodeName.camelToSnakeCase()}"
                        }
                    }
                }

                val hasEnumChildren =
                    node.children.size == 2 && node.children[0].nodeType is NodeType.StringEnumeration

                // detect embedded enum types and generate a writer for them
                when (node.originalXmlNode?.xmlType) {
                    XmlType.SimpleType, XmlType.Attribute, is XmlType.Element -> {
                        if (hasEnumChildren) {
                            val sep = StringEnumWriter(
                                writerName = writerName + SIMPLE_SUFFIX,
                                dataType = node,
                            )

                            val generated = sep.generateWriter(
                                config,
                                primitiveWriters,
                                simpleWriters,
                                complexWriters
                            )

                            simpleWriters[node] = RustWriter(config.rustModule, sep.writerName)

                            target.println(generated)
                            logger.debug(generated)
                            return // TODO check correct
                        }
                    }

                    else -> Unit
                }


                val xmlType = node.originalXmlNode?.xmlType
                when {
                    xmlType is XmlType.SimpleType || xmlType is XmlType.Attribute -> {

                        // do not care if this is already done
                        if (simpleWriters.containsKey(node)) {
                            logger.warn("simple node $node already written")
                            return
                        }

                        when (hasEnumChildren) {
                            true -> {
                                // already processed
                            }

                            false -> {

                                // this message is a container for one parameter
                                check(node.children.size == 1)

                                val parameter = node.children[0]
                                val parameterNodeType = parameter.nodeType as NodeType.Parameter


                                // find writer for the primitive type we're using
                                val parameterType = checkNotNull(parameterNodeType.parameterType.parent) {
                                    "parameter type for node $node was null, wat?"
                                }
//                                    val primitiveWriterName = checkNotNull(simpleWriters[parameterType]) {
//                                        "primitive writer for $parameterType missing"
//                                    }

                                val simpleTypeWriter = SimpleTypeWriter(
                                    writerName = writerName + SIMPLE_SUFFIX,
                                    dataType = node,
                                    primitiveWriter = primitiveWriters[parameterType],
                                    simpleWriter = simpleWriters[parameterType]
                                )

                                val generated = simpleTypeWriter.generateWriter(
                                    config, primitiveWriters,
                                    simpleWriters,
                                    complexWriters
                                )

                                simpleWriters[node] = RustWriter(config.rustModule, simpleTypeWriter.writerName)
                                target.println(generated)
                                logger.debug(generated)
                            }
                        }
                    }

                    (xmlType is XmlType.Element || xmlType is XmlType.ComplexType) -> {
                        // do not care if this is already done
                        if (complexWriters.containsKey(node) && node.clusteredTypes == null) {
                            logger.warn("complex node $node already parsed")
                            return
                        }

                        // create writer for each nested type
                        val nestedMessages = node.children
                            .filter { it.nodeType is NodeType.Message }

                        nestedMessages.forEach { processNode(it, node, target) }

                        // find all types we're inheriting from
                        val ancestors = nodeType.extensionBaseNode?.let { RustQuickXmlParser.collectAncestors(it) }
                            ?: emptyList()
                        logger.info { "Ancestors for $node are $ancestors" }

                        // collect attribute parameters
                        val attributes = (ancestors + listOf(node)).map {
                            Pair(it,
                                it.children.filter { it.nodeType is NodeType.Parameter && (it.nodeType as NodeType.Parameter).wasAttribute }
                                    .toList()
                            )
                        }
                            .filter { it.second.isNotEmpty() }
                            .toList()

                        val attributeWithPath: List<Pair<String, BaseNode>> =
                            attributes.flatMap { (parent, attributes) ->
                                // if this a node from an ancestor, concat the path up to the last element
                                // we need to traverse the ancestors reversed because that is the access path
                                val revAncestors = listOf(node) + ancestors.reversed()
                                val prefix = accessAncestor(revAncestors, parent)!!

                                attributes.map {
                                    Pair(prefix, it)
                                }
                            }.flatMap { (prefix, attribute) ->

                                val nodeParameterType = (attribute.nodeType as NodeType.Parameter)
                                val nodeTypeRustStruct =
                                    attribute.languageType[OutputLanguage.Rust]!! as RustEntry.RustParameter

                                when (nodeParameterType.parameterType.parent?.originalXmlNode?.xmlType == XmlType.AttributeGroup) {
                                    // flatten
                                    false -> listOf(Pair("$prefix.${nodeTypeRustStruct.parameterName}", attribute))
                                    true -> {
                                        nodeParameterType.parameterType.parent!!.children.map {

                                            val dataTypeRustStruct =
                                                it.languageType[OutputLanguage.Rust]!! as RustEntry.RustParameter

                                            val newPrefix =
                                                "$prefix.${nodeTypeRustStruct.parameterName}.${dataTypeRustStruct.parameterName}"

                                            Pair(newPrefix, it)
                                        }
                                    }
                                }
                            }

                        val mappedAttributes = attributeWithPath
                            .map { (prefix, attribute) ->
                                val nodeParameterType = (attribute.nodeType as NodeType.Parameter)
                                val parent =
                                    checkNotNull(nodeParameterType.parameterType.parent) { "parent for parameter type missing" }
                                WriterAttributeInfo(
                                    attributeQName = attribute.originalXmlNode!!.qName!!,
                                    attributeTypeSimpleWriter = simpleWriters[parent],
                                    attributeTypePrimitiveWriter = primitiveWriters[parent],
                                    attributeType = nodeParameterType.parameterType.parent!!,
                                    isOptional = nodeParameterType.optional,
                                    isList = nodeParameterType.list,
                                    attributeOriginNode = attribute,
                                    attributeAccessPath = prefix
                                )
                            }


                        // determine if this element has content, i.e. if it extends a simple type or has simple content with an extension
                        val contentData: Triple<BaseNode, BaseNode, BaseNode>? = ancestors.find {
                            findContentTypeNode(it) != null
                        }?.let {
                            val (parameterNode, contentType) = findContentTypeNode(it)!!
                            Triple(it, parameterNode, contentType)
                        }
                            ?: findContentTypeNode(node)?.let { Triple(node, it.first, it.second) }
                            ?: run {
                                if (isUnionContainer(node)) {
                                    Triple(node, node.children[0], node.children[0])
                                } else {
                                    null
                                }
                            }
                            ?: run {
                                if (isEnumContainer(node)) {
                                    // the simple type parser also generated is our content type
                                    Triple(node, node, node) // TODO: unsure about (node, node, ..)
                                } else {
                                    null
                                }
                            }

                        val contentPath: String? = contentData?.let { content ->
                            getParameterAccessThroughAncestors(
                                listOf(node) + ancestors,
                                content.first,
                                content.second,
                            )?.ifBlank { null }
                        }

                        // determine if this element is only an element with a type
                        val typeNode: BaseNode? = getElementType(node)
                        typeNode?.let { logger.debug("node ${node.nodeName} has nodeType $typeNode") }

                        contentData?.let {
                            // find parser for content
                            check((simpleWriters[contentData.third] != null).xor(primitiveWriters[contentData.third] != null))
                            { "Could not find writer for simple type body $contentData for $node" }
                        }

                        val element = WriterElementInfo(
                            elementStepName = node.nodeName,
                            elementQName = node.originalXmlNode!!.qName!!,
                            elementTypeQName = node.originalXmlNode!!.qName!!,
                            elementType = typeNode,
                            elementTypeWriter = complexWriters[typeNode],
                            attributes = mappedAttributes,
                            isOptional = false,
                            isList = false,
                            elementNode = node,
                            ancestorTypes = ancestors,
                            contentWriterSimple = simpleWriters[contentData?.third],
                            contentWriterPrimitive = primitiveWriters[contentData?.third],
                            contentType = contentData?.third,
                            contentAccess = contentPath,
                        )

                        val writer = ElementWriter(
                            writerName = writerName + COMPLEX_SUFFIX,
                            dataType = node,
                            element = element,
                        )

                        val writerGenerated = writer.generateWriter(
                            config, primitiveWriters,
                            simpleWriters,
                            complexWriters
                        )

                        target.println(writerGenerated)
                        logger.debug(writerGenerated)

                        complexWriters[node] = RustWriter(config.rustModule, writer.writerName)

                    }

                    xmlType == null -> {
                        // check if it is a custom oneof
                        val isOneOf = node.children.size == 1 && node.children[0].nodeType is NodeType.OneOf

                        if (!isOneOf) {
                            TODO("Handle $node")
                        }
                        logger.info("Handling OneOf node $node")

                        val writer = OneOfContentWriter(
                            writerName = writerName,
                            dataType = node,
                        )

                        val writerGenerated = writer.generateWriter(
                            config, primitiveWriters,
                            simpleWriters,
                            complexWriters
                        )
                        target.println(writerGenerated)
                        logger.debug(writerGenerated)
                        complexWriters[node] = RustWriter(config.rustModule, writer.writerName)
                    }

                    else -> Unit
                }
            }

            is NodeType.BuiltinType -> TODO()
            is NodeType.OneOf -> TODO()
            is NodeType.Parameter -> TODO()
            NodeType.Root -> TODO()
            is NodeType.StringEnumeration -> TODO()
            null -> TODO()
        }
    }

    override fun episodeData(node: BaseNode): EpisodeData {
        return when {
            this.simpleWriters[node] != null -> {
                val writer = this.simpleWriters[node]!!
                EpisodeData.Mapper(
                    packageName = node.nodeName,
                    sourceToTargetFunction = "simple",
                    sourceStructName = writer.module.orEmpty(),
                    sourceTypeName = writer.name,
                    targetStructName = "",
                    targetToSourceFunction = "",
                    targetTypeName = ""
                )
            }

            this.complexWriters[node] != null -> {
                val writer = this.complexWriters[node]!!
                EpisodeData.Mapper(
                    packageName = node.nodeName,
                    sourceToTargetFunction = "complex",
                    sourceStructName = writer.module.orEmpty(),
                    sourceTypeName = writer.name,
                    targetStructName = "",
                    targetToSourceFunction = "",
                    targetTypeName = ""
                )
            }

            else -> EpisodeData.Model(packageName = "", typeName = "")
        }
    }

    override fun pluginIdentifier(): String {
        return "QuickXmlWriterV1"
    }

    override fun run() {
        val outputFolder = File(converterConfig.outputFolder, config.outputFolder)
        requireLegalFolderContents(outputFolder, RUST_FILE_EXTENSION)
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()

        val outputFile = File(outputFolder, config.outputFile)

        val os = ByteArrayOutputStream()
        PrintWriter(os).use { printWriter ->

            printWriter.println(WRITER_IMPORTS)
            printWriter.println(WRITER_DEFINITIONS)

            if (generateCustomTypes) {
                printWriter.println(simpleTypeQualifiedNameWriter(rustConfig.rustModule.replacePrefixWithCrate(config.modelRustModuleSubstitute)))

                config.extensionWriterCall?.let {
                    printWriter.println(
                        redirectExtensionWriter(
                            rustConfig.rustModule.replacePrefixWithCrate(config.modelRustModuleSubstitute),
                            it.fullyQualified(config.modelRustModuleSubstitute)
                        )
                    )
                } ?: run {
                    printWriter.println(extensionWriter(rustConfig.rustModule.replacePrefixWithCrate(config.modelRustModuleSubstitute)))
                }
                printWriter.println(dateOfBirthUnionWriter(rustConfig.rustModule.replacePrefixWithCrate(config.modelRustModuleSubstitute)))
            }

            builtinWriters.forEach { (_, _, parserCode) ->
                printWriter.println(parserCode)
            }

            filterTreeElements(tree, generateCustomTypes)
                .forEach { node ->
                    processNode(node, null, printWriter)
                }
        }

        outputFile.writeText(os.toString())
    }

}