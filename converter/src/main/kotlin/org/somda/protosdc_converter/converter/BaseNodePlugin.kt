package org.somda.protosdc_converter.converter

import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.EpisodeData

/**
 * Plugin interface.
 *
 * A plugin operates on a [BaseNode] structure and may modify its state.
 *
 * The structure is processed by invoking [run].
 * Only afterwards it is safe to call [episodeData].
 */
interface BaseNodePlugin: Runnable {

    /**
     * Queries the plugin for episode data.
     *
     * *Caution:* May only provide useful output after calling [run].
     *
     * @param node the node for which to retrieve episode data. How episode data is used depends on the plugin.
     */
    fun episodeData(node: BaseNode): EpisodeData

    /**
     * Returns the identifier for the plugin, which may include versioning information.
     */
    fun pluginIdentifier(): String
}