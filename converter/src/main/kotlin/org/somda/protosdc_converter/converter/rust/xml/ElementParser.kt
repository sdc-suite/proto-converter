package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.BORING_EVENTS
import org.somda.protosdc_converter.converter.rust.EVENT
import org.somda.protosdc_converter.converter.rust.LOCAL_BUFFER
import org.somda.protosdc_converter.converter.rust.QuickXmlParserConfig
import org.somda.protosdc_converter.converter.rust.READER
import org.somda.protosdc_converter.converter.rust.READER_READ_FUNC
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.INDENT
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustOption
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustVec
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.typeName
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.CONTENT_FIELD_NAME
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.ROOT_STATE
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.createBox
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.isOptionalContent
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.toEnd
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.toStart
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.validateName
import org.somda.protosdc_converter.converter.rust.TAG_NAME
import org.somda.protosdc_converter.converter.rust.XSI_TYPE_CONST_NAME
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.boundNamespace
import org.somda.protosdc_converter.converter.rust.camelToSnakeCase
import org.somda.protosdc_converter.converter.rust.elementParserCall
import org.somda.protosdc_converter.converter.rust.elementParserSignature
import org.somda.protosdc_converter.converter.rust.isChoiceContainer
import org.somda.protosdc_converter.converter.rust.isContentParameter
import org.somda.protosdc_converter.converter.rust.isEnumContainer
import org.somda.protosdc_converter.converter.rust.isUnionContainerType
import org.somda.protosdc_converter.converter.rust.parserErrorInvalidAttr
import org.somda.protosdc_converter.converter.rust.parserErrorMandatoryContentMissing
import org.somda.protosdc_converter.converter.rust.parserErrorMandatoryElementMissing
import org.somda.protosdc_converter.converter.rust.parserErrorQuickXmlError
import org.somda.protosdc_converter.converter.rust.parserErrorUnexpectedAttribute
import org.somda.protosdc_converter.converter.rust.parserErrorUnexpectedEof
import org.somda.protosdc_converter.converter.rust.parserErrorUnexpectedParserEndState
import org.somda.protosdc_converter.converter.rust.parserErrorUnexpectedParserStartState
import org.somda.protosdc_converter.converter.rust.parserErrorUnexpectedParserTextEventState
import org.somda.protosdc_converter.converter.rust.primitiveParserCall
import org.somda.protosdc_converter.converter.rust.quickXmlQname
import org.somda.protosdc_converter.converter.rust.rustToStringCall
import org.somda.protosdc_converter.converter.rust.simpleParserCall
import org.somda.protosdc_converter.converter.rust.unboundNamespace
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.BuiltinTypes
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage
import org.somda.protosdc_converter.xmlprocessor.XmlType


data class ElementParser(
    val parserName: String,
    val dataType: BaseNode,
    val element: ParserElementInfo,
    val rustModulePath: String,
) : ParserTracker {

    companion object {
        const val ENUM_NAME = "State"
        const val STATE_VARIABLE_NAME = "state"
        const val ATTRIBUTE_VARIABLE_SUFFIX = "_attr"

        fun statePrefix(stateName: String) = "$ENUM_NAME::$stateName"
    }

    override fun generateParser(
        config: QuickXmlParserConfig,
        primitiveParsers: MutableMap<BaseNode, RustParser>,
        simpleParsers: MutableMap<BaseNode, RustParser>,
        complexParsers: MutableMap<BaseNode, RustParser>
    ): String {
        val targetType = dataType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct
        val targetTypeName = targetType.fullyQualifiedName(config.modelRustModuleSubstitute)

        val functionStart = elementParserSignature(targetTypeName, config.extensionAssociatedType)
        val functionEnd = "\n${INDENT.repeat(1)}}\n}"
        // generate state machine
        val stateMachineStart = """
${INDENT.repeat(2)}#[derive(Clone, Copy, Debug)]
${INDENT.repeat(2)}enum $ENUM_NAME {
${INDENT.repeat(3)}$ROOT_STATE,"""

        val states = element.getStates().filter { it != ROOT_STATE }.joinToString(separator = "") {
            """
${INDENT.repeat(3)}$it,"""
        }

        val stateMachineEnd = """
${INDENT.repeat(2)}}"""

        val stateMachine = stateMachineStart + states + stateMachineEnd

        val stateVariable = """
${INDENT.repeat(2)}let mut $STATE_VARIABLE_NAME = ${statePrefix(ROOT_STATE)};"""

        // attribute fields

        // Attribute, variable name
        val processedAttributes: Map<ParserAttributeInfo, String> = element.attributes.associateWith {
            val fieldName = validateName(it.attributeQName.localPart.camelToSnakeCase() + ATTRIBUTE_VARIABLE_SUFFIX)
            fieldName
        }

        val attributeFieldsComment = "\n\n${INDENT.repeat(2)}// attribute fields"
        val attributeFields = processedAttributes.entries.joinToString(separator = "") { (attr, fieldName) ->
            val attributeTargetType =
                attr.attributeType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct

            val attributeTargetTypeName = when (attributeTargetType.builtinType) {
                true -> typeName(attributeTargetType, rustModule = attributeTargetType.rustModule)
                false -> attributeTargetType.fullyQualifiedName(config.modelRustModuleSubstitute)
            }

            "\n${INDENT.repeat(2)}let mut $fieldName: Option<${attributeTargetTypeName}> = None;"
        }


        // child element fields
        val processedElements: Map<ParserElementInfo, String> = element.children?.flatMapIndexed { idx, coll ->

            coll.elements
                // filter ancestor elements
                .filter { !element.isAncestorType(it) }
                // filter ancestor content types, they're in content field
                .filter { (idx == 0 && !isContentParameter(it.elementNode)) || idx != 0 }.map {
                    val fieldName = validateName(it.elementQName.localPart.camelToSnakeCase())
                    it to fieldName
                }
        }?.toMap() ?: emptyMap()

        val elementFieldsComment = "\n\n${INDENT.repeat(2)}// element fields"
        val elementFields = processedElements.map { (elem, fieldName) ->
            val parameterNode = elem.elementNode
            val parameterType = (parameterNode.nodeType as NodeType.Parameter)
            val elementTargetType =
                parameterType.parameterType.parent!!.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct
            val elementTargetTypeName = when (elementTargetType.builtinType) {
                true -> typeName(elementTargetType, rustModule = elementTargetType.rustModule)
                false -> elementTargetType.fullyQualifiedName(config.modelRustModuleSubstitute)
            }

            when (parameterType.list) {
                true -> "\n${INDENT.repeat(2)}let mut $fieldName: ${rustVec(elementTargetTypeName)} = vec![];"
                false -> "\n${INDENT.repeat(2)}let mut $fieldName: ${rustOption(elementTargetTypeName)} = None;"
            }
        }.joinToString(separator = "")

        // content fields
        val contentFieldComment = "\n\n${INDENT.repeat(2)}// content field"
        val contentField = element.contentType?.let {
            // content type
            val elementTargetTypeName = element.contentTypeName(config, dataType)
            "\n${INDENT.repeat(2)}let mut $CONTENT_FIELD_NAME: Option<${elementTargetTypeName}> = None;\n"

        } ?: "\n${INDENT.repeat(2)}// no content\n"


        // parse attributes
        val attributeParser = generateAttributeParser(processedAttributes, element.elementTypeParser != null)

        // set state and create buffer
        val setStateAndBuffer =
            "\n${INDENT.repeat(2)}$STATE_VARIABLE_NAME = ${statePrefix(element.getStates()[1])};\n        let mut $LOCAL_BUFFER = vec![];"

        // parse elements
        val elementsParser = element.elementTypeParser?.let { generateElementTypeParser(config) }
        val contentParser = generateElementsParser(
            config,
            processedAttributes,
            processedElements,
            primitiveParsers,
            simpleParsers,
            complexParsers
        )

        val elements = when (elementsParser == null) {
            true -> setStateAndBuffer + contentParser
            false -> elementsParser
        }

        return functionStart + stateMachine + stateVariable + attributeFieldsComment + attributeFields + elementFieldsComment + elementFields + contentFieldComment + contentField + attributeParser + elements + functionEnd
    }

    private fun generateAttributeParser(attributes: Map<ParserAttributeInfo, String>, acceptUnknown: Boolean): String {
        val attributeVariable = "attribute"
        val start = """
${INDENT.repeat(2)}for attr in $EVENT.attributes() {
${INDENT.repeat(3)}let $attributeVariable = attr.map_err(|err| ${parserErrorQuickXmlError(parserErrorInvalidAttr("err"))})?;
${INDENT.repeat(3)}match $attributeVariable.key.0 {"""

        val middle = attributes.entries
            .filter { it.key.attributeQName.namespaceURI.isEmpty() }
            .joinToString(separator = "") { (it, fieldName) ->
                // check that we only have attributes with no namespace here
                check(it.attributeQName.namespaceURI.isEmpty()) {
                    "Attribute $it had a namespace, what's up with that?"
                }
                "\n${INDENT.repeat(4)}b\"${it.attributeQName.localPart}\" => {\n" +
                    "${INDENT.repeat(5)}$fieldName = Some(${
                        it.parserCall(
                            "&$attributeVariable.unescape_value().map_err(|it| ${
                                parserErrorQuickXmlError(
                                    "it"
                                )
                            })?.as_bytes()"
                        )
                    }?)\n" +
                    "${INDENT.repeat(4)}},"
            }

        val expectedQname: String? = element.elementTypeQName?.let {
            when (it.namespaceURI.isNotEmpty()) {
                true -> {
                    "(${boundNamespace("b\"${it.namespaceURI}\"")}, b\"${it.localPart}\")"
                }

                false -> {
                    "($unboundNamespace, b\"${it.localPart}\")"
                }
            }
        }

        val fallThrough = "_ => {} // fall through"
        val unknownAttributeError = "Err(${
            parserErrorUnexpectedAttribute(
                "attribute_to_string($EVENT.local_name().into_inner())",
                "attribute_to_string($attributeVariable.key.0)",
                "attribute_to_string(&$attributeVariable.value)"
            )
        })?"

        val qualifiedAttributes = attributes.entries
            .filter { it.key.attributeQName.namespaceURI.isNotEmpty() }
            .joinToString(separator = "") { (it, fieldName) ->
                // check that we only have attributes with namespace here
                check(it.attributeQName.namespaceURI.isNotEmpty()) {
                    "Attribute $it had no namespace, what's up with that?"
                }

                "\n${INDENT.repeat(6)}(${boundNamespace("b\"${it.attributeQName.namespaceURI}\"")}, b\"${it.attributeQName.localPart}\") => {\n" +
                    "${INDENT.repeat(7)}$fieldName = Some(${
                        it.parserCall(
                            "&$attributeVariable.unescape_value().map_err(|it| ${
                                parserErrorQuickXmlError(
                                    "it"
                                )
                            })?.as_bytes()"
                        )
                    }?);\n" +
                    "${INDENT.repeat(7)}continue\n" +
                    "${INDENT.repeat(6)}},"
            }

        val end = """
                _ => {
                    // ignore xmlns attributes, they're namespace definitions
                    if $attributeVariable.key.0.starts_with(b"xmlns:") || $attributeVariable.key.0 == b"xmlns" {
                        continue
                    }
                    // handle qualified namespace
                    let resolved = $READER.resolve_attribute($attributeVariable.key);
                    match (resolved.0, resolved.1.into_inner()) {
                        $XSI_TYPE_CONST_NAME => {
                            // verify type is correct
                            let resolved_value = $READER.resolve_element($quickXmlQname(&$attributeVariable.value));
                            match (resolved_value.0, resolved_value.1.into_inner()) {
""" +
            (expectedQname?.let {
                "                                $it => {\n" +
                    "                                    continue\n" +
                    "                                }"

            } ?: "") +
            """
                                $fallThrough
                            }
                        }
                        $qualifiedAttributes
                        $fallThrough
                    }
                    ${if (!acceptUnknown) unknownAttributeError else ""}
                }
            }
        };
"""
        return start + middle + end
    }

    private fun generateElementTypeParser(config: QuickXmlParserConfig): String {
        element.elementTypeParser
        val rustStructType = element.elementType!!.languageType[OutputLanguage.Rust] as RustEntry.RustStruct

        val fullyQualifiedRustTypeName = rustStructType.fullyQualifiedName(config.modelRustModuleSubstitute)

        val field = element.elementNode.children[0].languageType[OutputLanguage.Rust] as RustEntry.RustParameter
        val fieldName = field.parameterName

        return """
        return Ok(Self { $fieldName: ${elementParserCall(fullyQualifiedRustTypeName, TAG_NAME, EVENT)}? })
            """
    }

    private fun generateElementsParser(
        config: QuickXmlParserConfig,
        attributes: Map<ParserAttributeInfo, String>,
        elements: Map<ParserElementInfo, String>,
        primitiveParsers: MutableMap<BaseNode, RustParser>,
        simpleParsers: MutableMap<BaseNode, RustParser>,
        complexParsers: MutableMap<BaseNode, RustParser>
    ): String {

        val loopStart = """
        loop {
            match $READER.$READER_READ_FUNC(&mut $LOCAL_BUFFER) {"""

        val loopEnd = """
${generateTextParser(config, elements, primitiveParsers, simpleParsers)}
${BORING_EVENTS.joinToString(separator = "\n") { "${INDENT.repeat(4)}Ok((_, $it)) => {}," }}
                Ok((_, Event::Eof)) => Err(${parserErrorUnexpectedEof()})?,
                Err(err) => Err(${parserErrorQuickXmlError("err")})?,
            }
        }
            """

        val startEventStart = """
                Ok((ref ns, Event::Start(ref e))) => {
                    match ($STATE_VARIABLE_NAME, ns, e.local_name().as_ref()) { 
"""
        val startEventEnd = """
                        _ => Err(${
            parserErrorUnexpectedParserStartState(
                "match String::from_utf8(e.local_name().into_inner().to_vec()) { Ok(it) => it, Err(_) => ${
                    rustToStringCall(
                        "FromUtf8Error"
                    )
                } }", "format!(\"{:?}\", $STATE_VARIABLE_NAME)"
            )
        })?
                    }
                }
"""

        val endEventStart = """
                Ok((ref ns, Event::End(ref e))) => {
                    match ($STATE_VARIABLE_NAME, ns, e.local_name().as_ref()) {
"""

        val endEventEnd = """
                        _ => Err(${
            parserErrorUnexpectedParserEndState(
                "match String::from_utf8(e.local_name().into_inner().to_vec()) { Ok(it) => it, Err(_) => ${
                    rustToStringCall(
                        "FromUtf8Error"
                    )
                } }", "format!(\"{:?}\", $STATE_VARIABLE_NAME)"
            )
        })?
                    }
                }
"""

        val elementParserEvents: String = element.getFilteredChildren().map {
            // valid transitions
            val transitions = element.getValidTransitionsForChild(it)

            check(transitions.isNotEmpty()) { "Cannot determine valid input transitions for $it" }

            // find parser call for child
            val fieldName = checkNotNull(elements[it]) { "Could not find fieldName for $it" }
            val targetType = (it.elementNode.nodeType as NodeType.Parameter).parameterType.parent!!

            val rustStructType = targetType.languageType[OutputLanguage.Rust] as RustEntry.RustStruct
            val fullyQualifiedRustTypeName = rustStructType.fullyQualifiedName(config.modelRustModuleSubstitute)

            val isPrimitiveTypeElement = primitiveParsers[targetType] != null
            val isSimpleTypeElement = simpleParsers[targetType] != null

            val inputTransitions = transitions.joinToString(separator = " | ") { state ->
                "(${statePrefix(state)}, ${boundNamespace("b\"${it.elementQName.namespaceURI}\"")}, b\"${it.elementQName.localPart}\")"
            }

            val startEventBranchStart = """
                        $inputTransitions => {
                            $STATE_VARIABLE_NAME = ${statePrefix(toStart(it.elementStepName))};"""

            val elementParserCall = complexParsers[targetType]?.let { _ ->
                val typeQname = when {
                    it.elementQName.namespaceURI.isNotBlank() -> "(${boundNamespace("b\"${it.elementQName.namespaceURI}\"")}, b\"${it.elementQName.localPart}\")"
                    else -> "($unboundNamespace, b\"${it.elementQName.localPart}\")"
                }

                "${elementParserCall(fullyQualifiedRustTypeName, typeQname, "e")}?"
            }

            val startEventBranchParserCall = when (isSimpleTypeElement || isPrimitiveTypeElement) {
                false -> {
                    checkNotNull(elementParserCall) {
                        "no parser for not simple type $targetType"
                    }
                    "\n${INDENT.repeat(7)}" + when {
                        !it.isList -> "$fieldName = Some($elementParserCall);"
                        it.isList -> "$fieldName.push($elementParserCall);"
                        else -> TODO("Unreachable?!")
                    }
                }
                // text content parser will do stuff
                true -> ""
            }


            val startEventBranchEnd = when (isSimpleTypeElement || isPrimitiveTypeElement) {
                false -> {
                    """
                                $STATE_VARIABLE_NAME = ${statePrefix(toEnd(it.elementStepName))};
                            },
"""
                }
                // requires proper end element handling since no sub parser is taking care of it
                true -> "\n${INDENT.repeat(5)}},\n"
            }

            startEventBranchStart + startEventBranchParserCall + startEventBranchEnd
        }.joinToString(separator = "")

        val elementEndEvents: String = element.getFilteredChildren().filter {
            // find parser call for child
            val targetType = (it.elementNode.nodeType as NodeType.Parameter).parameterType.parent!!

            simpleParsers[targetType] != null || primitiveParsers[targetType] != null
        }.map {

            val elementStartStateName = statePrefix(toStart(it.elementStepName))
            val elementEndStateName = statePrefix(toEnd(it.elementStepName))

            val iBase = 5
            val endHandling =
                "${INDENT.repeat(iBase)}($elementStartStateName, ${boundNamespace("b\"${it.elementQName.namespaceURI}\"")}, b\"${it.elementQName.localPart}\") => {\n" +
                    "${INDENT.repeat(iBase + 1)}$STATE_VARIABLE_NAME = $elementEndStateName;\n" +
                    "${INDENT.repeat(iBase)}},\n"

            endHandling
        }.joinToString(separator = "")

        val elementEndEventStart = """
                        (_, ns, local_name) if &$TAG_NAME.0 == ns && $TAG_NAME.1 == local_name.as_ref() => {
                            return Ok("""
        val elementEndEventEnd = """);
                        },"""

        fun constructInheritedType(node: BaseNode, indent: Int): String {
            check(node.nodeType is NodeType.Message)
            val rustStructType = node.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct

            val constructedTypeFullyQualifiedTypeName =
                rustStructType.fullyQualifiedName(config.modelRustModuleSubstitute)

            val start = "$constructedTypeFullyQualifiedTypeName {"
            val end = "\n${INDENT.repeat(indent)}},"

            val parameters = node.children
                .filter { it.nodeType is NodeType.Parameter }
                .map {
                    val parameterType = it.nodeType as NodeType.Parameter
                    val rustParameterType = it.languageType[OutputLanguage.Rust]!! as RustEntry.RustParameter

                    val fieldStart = "\n${INDENT.repeat(indent + 1)}${rustParameterType.parameterName}: "

                    val targetWasAttributeGroup: Boolean =
                        parameterType.parameterType.parent?.originalXmlNode?.xmlType == XmlType.AttributeGroup

                    val fieldValue = when {
                        // TODO: Use isContent from above
                        // if we inherited, and it was NOT a simple type we inherited from i.e. no content, do this
                        (parameterType.inheritance && parameterType.parameterType.parent?.originalXmlNode?.xmlType != XmlType.SimpleType) || targetWasAttributeGroup -> {
                            when (rustParameterType.box) {
                                false -> constructInheritedType(parameterType.parameterType.parent!!, indent + 1)
                                true -> "${
                                    createBox(
                                        constructInheritedType(
                                            parameterType.parameterType.parent!!,
                                            indent + 1
                                        )
                                    )
                                },"
                            }
                        }

                        else -> {
                            // find field name
                            val attributeFieldName =
                                attributes.keys.find { attr -> attr.attributeOriginNode === it }
                                    ?.let { attr -> attributes[attr]!! }
                            val elementFieldName =
                                elements.keys.find { ele -> ele.elementNode === it }?.let { ele -> elements[ele]!! }
                            val isContentField = it.nodeType is NodeType.Parameter && isContentParameter(it)

                            val fieldName: String = attributeFieldName ?: elementFieldName
                            ?: if (isContentField) CONTENT_FIELD_NAME else null
                                ?: throw IllegalArgumentException("No field name known for $it")

                            // lookup element field name and assign
                            val fieldContent: String = when {
                                ((isContentField && isOptionalContent(it)) || parameterType.optional) && !parameterType.list || ((isContentField && isOptionalContent(
                                    it
                                )) || parameterType.optional) && parameterType.list -> {
                                    when (rustParameterType.box) {
                                        true -> "match $fieldName { Some(x) => Some(${createBox("x")}), None => None },"
                                        false -> when (isContentField) {
                                            true -> when (isOptionalContent(it)) {
                                                false -> "$fieldName.ok_or(${
                                                    parserErrorMandatoryContentMissing(
                                                        rustToStringCall(constructedTypeFullyQualifiedTypeName)
                                                    )
                                                }),"

                                                true -> "$fieldName.unwrap_or_default(),"
                                            }

                                            false -> "$fieldName,"
                                        }
                                    }
                                }

                                !parameterType.list -> {

                                    // if this is just a string, it can be empty. We can just default here,
                                    // because the structure, i.e. the presence of at least the empty element,
                                    // is enforced by the state machine
                                    val base =
                                        when (parameterType.parameterType is NodeType.BuiltinType && (parameterType.parameterType as NodeType.BuiltinType).origin == BuiltinTypes.XSD_STRING.qname) {
                                            true -> "$fieldName.unwrap_or_default()"
                                            false -> {
                                                "$fieldName.ok_or(${
                                                    parserErrorMandatoryElementMissing(
                                                        rustToStringCall(it.nodeName)
                                                    )
                                                })?"
                                            }
                                        }

                                    this.element.contentType

                                    when (rustParameterType.box) {
                                        true -> "${createBox(base)},"
                                        false -> "$base,"
                                    }
                                }

                                parameterType.list -> {
                                    "match $fieldName.is_empty() {\n" +
                                        "${INDENT.repeat(indent + 2)}true => Err(${
                                            parserErrorMandatoryElementMissing(
                                                rustToStringCall(fieldName)
                                            )
                                        })?,\n" +
                                        "${INDENT.repeat(indent + 2)}false => $fieldName,\n" +
                                        "${INDENT.repeat(indent + 1)}},"
                                }

                                else -> {
                                    throw IllegalArgumentException("No field handling known for $it")
                                }
                            }

                            fieldContent
                        }
                    }

                    fieldStart + fieldValue
                }

            return start + parameters.joinToString(separator = "") + end
        }

        val elementEndEventMiddle: String = when {
            !isChoiceContainer(element) && !isEnumContainer(element.elementNode) && !isUnionContainerType(element.elementNode) -> constructInheritedType(
                element.elementNode,
                indent = 7
            )

            isChoiceContainer(element) -> {
                val choiceType = element.elementNode.languageType[OutputLanguage.Rust] as RustEntry.RustStruct
                val choiceTypeName = choiceType.fullyQualifiedName(config.modelRustModuleSubstitute)

                val children = checkNotNull(element.children?.get(0)) as ChildCollection.Choice

                val response = children.elements.map {
                    val elementFieldName = elements.keys.find { ele -> ele === it }?.let { ele -> elements[ele]!! }

                    "match $elementFieldName {" +
                        "Some(x) => $choiceTypeName::${it.elementNode.nodeName}(x)," +
                        "None => "
                }
                    .joinToString(separator = "") +
                    "Err(${
                        parserErrorMandatoryElementMissing(
                            rustToStringCall("Any Choice for Choice ${element.elementQName}")
                        )
                    })?" +
                    // minus one as we already provided brace for last element
                    "}".repeat(children.elements.size) +
                    ""

                response
            }

            isEnumContainer(element.elementNode) -> "$CONTENT_FIELD_NAME.ok_or(${
                parserErrorMandatoryElementMissing(
                    rustToStringCall(CONTENT_FIELD_NAME)
                )
            })?"

            else -> "$CONTENT_FIELD_NAME.ok_or(${
                parserErrorMandatoryElementMissing(
                    rustToStringCall(CONTENT_FIELD_NAME)
                )
            })?"
        }

        return loopStart + startEventStart + elementParserEvents + startEventEnd + endEventStart + elementEndEvents + elementEndEventStart + elementEndEventMiddle + elementEndEventEnd + endEventEnd + loopEnd
    }

    private fun generateTextParser(
        config: QuickXmlParserConfig,
        elements: Map<ParserElementInfo, String>,
        primitiveParsers: MutableMap<BaseNode, RustParser>,
        simpleParsers: MutableMap<BaseNode, RustParser>,
    ): String {

        val indentBase = 4

        val textMatchArmStart = "${INDENT.repeat(indentBase)}Ok((_, Event::Text(ref e))) => "
        val cDataTextMatchArmStart = "${INDENT.repeat(indentBase)}Ok((_, Event::CData(ref e))) => "

        val emptyArm = "{}"

        // determine if we need to do anything because of children
        val hasSimpleTypeChildElement = element.children?.any { collection ->
            collection.elements.any {
                // find parser call for child
                val targetType = (it.elementNode.nodeType as NodeType.Parameter).parameterType.parent!!

                simpleParsers[targetType] != null || primitiveParsers[targetType] != null
            }
        } ?: false

        if (!hasSimpleTypeChildElement && element.contentParserSimple == null && element.contentParserPrimitive == null) {
            return textMatchArmStart + emptyArm + "\n" + cDataTextMatchArmStart + emptyArm
        }

        val textMatchBegin = "$textMatchArmStart{\n" +
            "${INDENT.repeat(indentBase + 1)}match $STATE_VARIABLE_NAME {\n"
        val textMatchEnd =
            "${INDENT.repeat(indentBase + 2)}_ => Err(${parserErrorUnexpectedParserTextEventState("format!(\"{:?}\", $STATE_VARIABLE_NAME)")})?\n" +
                "${INDENT.repeat(indentBase + 1)}}\n" +
                "${INDENT.repeat(indentBase)}}"

        val cDataTextMatchBegin = "$cDataTextMatchArmStart{\n" +
            "${INDENT.repeat(indentBase + 1)}match $STATE_VARIABLE_NAME {\n"


        var textMatchMiddle = ""
        var cDataTextMatchMiddle = ""

        val (contentParser, cDataContentParser) = when {
            element.contentParserPrimitive != null || element.contentParserSimple != null -> run {
                val elementStartStateName = statePrefix(toStart(element.elementStepName))

                val parserCall: (String) -> (String) = when {
                    element.contentParserPrimitive != null -> { variable ->
                        primitiveParserCall(
                            element.contentParserPrimitive,
                            variable
                        )
                    }

                    element.contentParserSimple != null -> { variable ->
                        simpleParserCall(
                            element.contentTypeName(
                                config,
                                dataType
                            ), variable
                        )
                    }

                    else -> TODO("Parser for ${element.contentType}")
                }


                val fieldName = CONTENT_FIELD_NAME

                val matchArmStart = "${INDENT.repeat(indentBase + 2)}$elementStartStateName => {\n"
                val matchArmEnd = "${INDENT.repeat(indentBase + 2)}}\n"

                val parserCallSegment =
                    "${parserCall("&e.unescape().map_err(|err| ${parserErrorQuickXmlError("err")})?.as_bytes()")}?"

                // TODO: This is a poor solution, get rid of the clone
                val cDataParserCallSegment = "${
                    parserCall(
                        "e.clone().escape().map_err(|err| ${parserErrorQuickXmlError("err")})?.unescape().map_err(|err| ${
                            parserErrorQuickXmlError("err")
                        })?.as_bytes()"
                    )
                }?"

                val matchArmMiddle = INDENT.repeat(indentBase + 3) + when {
                    !element.isList -> "$fieldName = Some($parserCallSegment);"
                    element.isList -> "$fieldName.push($parserCallSegment);"
                    else -> TODO("Unreachable?!")
                } + "\n"

                val cDataMatchArmMiddle = INDENT.repeat(indentBase + 3) + when {
                    !element.isList -> "$fieldName = Some($cDataParserCallSegment);"
                    element.isList -> "$fieldName.push($cDataParserCallSegment);"
                    else -> TODO("Unreachable?!")
                } + "\n"

                Pair(
                    matchArmStart + matchArmMiddle + matchArmEnd,
                    matchArmStart + cDataMatchArmMiddle + matchArmEnd
                )
            }

            else -> Pair("", "")
        }

        textMatchMiddle += contentParser
        cDataTextMatchMiddle += cDataContentParser

        // TODO: CDATA support

        fun mapChildTextParsers(
            it: ParserElementInfo,
            parserCallInner: String
        ): String {
            // find parser call for child
            val fieldName = elements[it]!!
            val targetType = (it.elementNode.nodeType as NodeType.Parameter).parameterType.parent!!

            val fullyQualifiedRustTypeName = when (val rustType = targetType.languageType[OutputLanguage.Rust]) {
                is RustEntry.RustStruct -> rustType.fullyQualifiedName(config.modelRustModuleSubstitute)
                is RustEntry.RustOneOf -> rustType.fullyQualifiedName(config.modelRustModuleSubstitute)
                else -> TODO("$rustType")
            }

            val parserCall: (String) -> (String) = when {
                it.contentParserPrimitive != null -> { variable ->
                    primitiveParserCall(
                        it.contentParserPrimitive,
                        variable
                    )
                }

                it.contentParserSimple != null -> { variable ->
                    simpleParserCall(
                        fullyQualifiedRustTypeName,
                        variable
                    )
                }

                else -> TODO("Parser for ${it.contentType}")
            }

            val parserCallSegment = "${parserCall(parserCallInner)}?"

            val elementStartStateName = statePrefix(toStart(it.elementStepName))

            val matchArmStart = "${INDENT.repeat(indentBase + 2)}$elementStartStateName => {\n"
            val matchArmEnd = "${INDENT.repeat(indentBase + 2)}}\n"

            val matchArmMiddle = INDENT.repeat(indentBase + 3) + when {
                !it.isList -> "$fieldName = Some($parserCallSegment);"
                it.isList -> "$fieldName.push($parserCallSegment);"
                else -> TODO("Unreachable?!")
            } + "\n"

            return matchArmStart + matchArmMiddle + matchArmEnd
        }

        val parserCallSegment = "&e.unescape().map_err(|err| ${parserErrorQuickXmlError("err")})?.as_bytes()"
        val cDataParserCallSegment =
            "e.clone().escape().map_err(|err| ${parserErrorQuickXmlError("err")})?.unescape().map_err(|err| ${
                parserErrorQuickXmlError("err")
            })?.as_bytes()"

        val childTextParsers: String = element.getFilteredChildren().filter {
            // find parser call for child
            val targetType = (it.elementNode.nodeType as NodeType.Parameter).parameterType.parent!!

            // TODO: Filter simple type ancestors without fields, we don't need to parse them
            simpleParsers[targetType] != null || primitiveParsers[targetType] != null
        }.map { mapChildTextParsers(it, parserCallSegment) }
            .joinToString(separator = "")

        val cDataChildTextParsers: String = element.getFilteredChildren().filter {
            // find parser call for child
            val targetType = (it.elementNode.nodeType as NodeType.Parameter).parameterType.parent!!

            // TODO: Filter simple type ancestors without fields, we don't need to parse them
            simpleParsers[targetType] != null || primitiveParsers[targetType] != null
        }.map { mapChildTextParsers(it, cDataParserCallSegment) }
            .joinToString(separator = "")

        textMatchMiddle += childTextParsers
        cDataTextMatchMiddle += cDataChildTextParsers

        return textMatchBegin + textMatchMiddle + textMatchEnd + "\n" + cDataTextMatchBegin + cDataTextMatchMiddle + textMatchEnd
    }
}
