package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.EVENT
import org.somda.protosdc_converter.converter.rust.QuickXmlParserConfig
import org.somda.protosdc_converter.converter.rust.READER
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.INDENT
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.MOD_SEP
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.createBox
import org.somda.protosdc_converter.converter.rust.RustQuickXmlParser.Companion.logger
import org.somda.protosdc_converter.converter.rust.TAG_NAME
import org.somda.protosdc_converter.converter.rust.XSI_TYPE_CONST_NAME
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.boundNamespace
import org.somda.protosdc_converter.converter.rust.elementParserCall
import org.somda.protosdc_converter.converter.rust.elementParserSignature
import org.somda.protosdc_converter.converter.rust.parserErrorInvalidAttr
import org.somda.protosdc_converter.converter.rust.parserErrorOneOfParserFallthrough
import org.somda.protosdc_converter.converter.rust.parserErrorOther
import org.somda.protosdc_converter.converter.rust.parserErrorQuickXmlError
import org.somda.protosdc_converter.converter.rust.quickXmlQname
import org.somda.protosdc_converter.converter.rust.rustToStringCall
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

data class OneOfParser(val parserName: String, val dataType: BaseNode, val rustModulePath: String) : ParserTracker {
    override fun generateParser(
        config: QuickXmlParserConfig,
        primitiveParsers: MutableMap<BaseNode, RustParser>,
        simpleParsers: MutableMap<BaseNode, RustParser>,
        complexParsers: MutableMap<BaseNode, RustParser>
    ): String {


        // collect parsers for each possible type
        val oneOfNode = dataType.children[0].nodeType as NodeType.OneOf
        val rustOneOfNode = dataType.children[0].languageType[OutputLanguage.Rust] as RustEntry.RustOneOf
        val oneOfElements = dataType.children[0].children

        val oneOfPrefix = rustOneOfNode.fullyQualifiedName(config.modelRustModuleSubstitute)

        // generate the return value for parsing a given OneOf - we need this twice, so a function is nice
        fun oneOfElementParser(element: BaseNode): String {

            val variantName = "$oneOfPrefix$MOD_SEP${element.nodeName}"

            val variantType = (element.nodeType as NodeType.Parameter).parameterType.parent!!
            val variantIsSimpleType = simpleParsers[variantType] != null

            val variantParser = complexParsers[variantType]

            val rustParameterType = (element.languageType[OutputLanguage.Rust] as RustEntry.RustOneOfParameter)
            val dataTypeRustStruct = rustParameterType.rustType as RustEntry.RustStruct

            val fullyQualifiedRustTypeName = dataTypeRustStruct.fullyQualifiedName(config.modelRustModuleSubstitute)

            return when (variantIsSimpleType) {
                true -> {
                    logger.warn("Simple type parser in OneOf, not handled!")
                    "return Err(${parserErrorOther()})"
                }

                false -> {
                    checkNotNull(variantParser) { "no parser known for $variantType" }
                    when (rustParameterType.box) {
                        false -> "return Ok($variantName(${
                            elementParserCall(
                                fullyQualifiedRustTypeName,
                                TAG_NAME,
                                EVENT
                            )
                        }?))"

                        true -> "return Ok($variantName(${
                            createBox(
                                "${
                                    elementParserCall(
                                        fullyQualifiedRustTypeName,
                                        TAG_NAME,
                                        EVENT
                                    )
                                }?"
                            )
                        }))"
                    }
                }
            }
        }

        val middle = oneOfElements.joinToString(separator = "") { element ->

            val variantType = (element.nodeType as NodeType.Parameter).parameterType.parent!!
            val variantTypeQname = variantType.originalXmlNode!!.qName!!

            val matchStart = """
${INDENT.repeat(5)}(${boundNamespace("b\"${variantTypeQname.namespaceURI}\"")}, b"${variantTypeQname.localPart}") => {
"""
            val matchEnd = """
${INDENT.repeat(5)}},
                """

            val matchMiddle = INDENT.repeat(6) + oneOfElementParser(element)

            matchStart + matchMiddle + matchEnd
        }


        val defaultParameterReturn = oneOfNode.defaultTypeParameter?.let {
            val lvl = 2
            INDENT.repeat(lvl) + "// default type as fallthrough\n" +
                INDENT.repeat(lvl) + oneOfElementParser(it)
        }

        val start = """
${elementParserSignature(oneOfPrefix, config.extensionAssociatedType)}
        // process initial event, trust that tag is already verified
        for attr in $EVENT.attributes() {
            let attribute = attr.map_err(|err| ${parserErrorQuickXmlError(parserErrorInvalidAttr("err"))})?;
            // ignore xmlns attributes, they're namespace definitions
            if attribute.key.0.starts_with(b"xmlns:") || attribute.key.0 == b"xmlns" {
                continue
            }
            // handle qualified namespace
            let resolved = $READER.resolve_attribute(attribute.key);
            match (resolved.0, resolved.1.into_inner()) {
                $XSI_TYPE_CONST_NAME => {
                    // verify type is correct
                    let resolved_value = $READER.resolve_element($quickXmlQname(&attribute.value));
                    match (resolved_value.0, resolved_value.1.into_inner()) {"""

        val end = """
                        _ => {} // fall through
                    }
                }
                _ => {} // fall through
            }
        }
${defaultParameterReturn ?: "         Err(${parserErrorOneOfParserFallthrough(rustToStringCall(parserName))})"}
}
}
            """

        return start + middle + end
    }
}
