package org.somda.protosdc_converter.converter.kotlin.entrygen

import org.somda.protosdc_converter.converter.kotlin.KotlinEntry

/**
 * Common interface for code generation of [KotlinEntry].
 */
internal interface CodeGenerator {
    /**
     * Generates a code preamble, defaults to empty string.
     */
    fun enter(): String = ""

    /**
     * Generates a code epilogue, defaults to empty string.
     */
    fun exit(): String = ""

    /**
     * Generates main content, defaults to empty string.
     */
    fun main(): String = ""
}