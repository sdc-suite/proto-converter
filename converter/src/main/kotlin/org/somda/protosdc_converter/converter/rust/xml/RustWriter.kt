package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.MOD_SEP
import org.somda.protosdc_converter.converter.rust.replacePrefixWithCrate


data class RustWriter(val module: String?, val name: String) {
    fun fullyQualifiedName(replacementPrefix: String? = null): String {
        val fqrn = "${module?.let { it + MOD_SEP } ?: ""}$name"
        return replacementPrefix?.let { fqrn.replacePrefixWithCrate(replacementPrefix) } ?: fqrn
    }
}