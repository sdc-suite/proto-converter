package org.somda.protosdc_converter.converter.kotlin

import com.akuleshov7.ktoml.Toml
import kotlinx.serialization.serializer
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.ConverterConfig
import org.somda.protosdc_converter.converter.BaseNodePlugin
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.ByteArrayOutputStream
import java.io.PrintWriter

class KotlinModelPlugin(
    private val tree: BaseNode,
    private val converterConfig: ConverterConfig,
    private val generateCustomTypes: Boolean = true,
    private val kotlinConfig: KotlinConfig,
    customTypes: KotlinTypeMap
) : BaseNodePlugin {
    private val kotlinSource = KotlinSource(
        kotlinConfig.indentationSize.toInt(),
        kotlinConfig.oneOfClassPrefix,
        kotlinConfig.oneOfClassMemberName
    )

    private val entryCodeGenerator = KotlinEntryCodeGenerator(kotlinSource)

    private val outputFolders = setupOutputFolders(
        converterConfig.outputFolder,
        kotlinConfig.outputFolders,
        kotlinConfig.outputFoldersNoHierarchy
    ) { "Output folders are missing for Kotlin generator" }

    init {
        // attach builtin types to their nodes
        KotlinBuiltInTypes().forEach { (typeQName, kotlinDataClass) ->
            val node = tree.findQName(typeQName)!!
            node.kotlinLanguageType(kotlinDataClass)
            logger.info("Attached builtin type $typeQName to kotlin type ${kotlinDataClass.typeName}")
        }
        // attach custom types to their nodes
        customTypes().forEach { (typeQName, kotlinDataClass) ->
            val node = tree.findQName(typeQName)!!
            node.kotlinLanguageType(kotlinDataClass)
            logger.info("Attached custom type $typeQName to kotlin type ${kotlinDataClass.typeName}")
        }
    }

    override fun pluginIdentifier() = KOTLIN_MODEL_PLUGIN_IDENTIFIER

    override fun run() {
        logger.info("Creating Kotlin model")

        // generate types for every "root" type, i.e. the ones on depth 1
        // every other type is nested within those and will be covered recursively
        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node ->
            attachKotlinEntryToBaseNode(node, null)
        }

        // restructure nesting to match kotlin code
        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node -> restructureNesting(node) }

        // determine imports for all nodes and
        val nodeImports = tree.children.associate { it.nodeId to determineImports(it) }


        // generate kotlin code
        filterOutIrrelevantBaseNodes(tree.children).forEach { node ->
            // determine file for this message
            val languageType = node.kotlinLanguageType() as KotlinEntry.DataClass
            checkNotNull(languageType as? KotlinEntry.DataClass) {
                "Expected ${KotlinEntry.DataClass::javaClass}, was ${node.kotlinLanguageType()}"
            }

            // determine directory structure required based on package
            val filesToWrite = determineFilesToWrite(
                outputFolders,
                kotlinConfig.kotlinPackage,
                languageType.fileNameFromType()
            )

            val byteStream = ByteArrayOutputStream()
            PrintWriter(byteStream).use { printWriter ->
                // add package declaration if present
                languageType.packagePath?.let {
                    printWriter.println(kotlinSource.packageDeclaration(it))
                    printWriter.println()
                }

                // add imports
                nodeImports[node.nodeId]?.let {
                    it.forEach { dependency -> printWriter.println(kotlinSource.importDeclaration(dependency)) }
                    printWriter.println()
                }

                entryCodeGenerator.generateDataClassCode(node, printWriter)
            }

            writeByteStreamToFile(byteStream, filesToWrite)
        }
    }

    private fun determineParameterName(node: BaseNode, parameter: NodeType.Parameter): String {
        // first letter must be lowercase
        return KotlinSource.toParameterName(
            when {
                parameter.wasAttribute -> "${node.nodeName}${converterConfig.attributeSuffix}"
                else -> node.nodeName
            }
        )
    }

    /**
     * Assigns children of a base node to the base node's [KotlinEntry.DataClass.nestedTypes] property.
     *
     * In addition, for each child that is also assigned as a nested type, [KotlinEntry.DataClass.onlyGenerateNested]
     * and [KotlinEntry.DataClass.skipType] are written accordingly.
     *
     * @param node the base node for which nested types are assigned.
     */
    private fun restructureNesting(node: BaseNode) {
        node.children.forEach {
            // fix nesting in children first, before we change things around
            restructureNesting(it)
        }

        when (val languageType = node.kotlinLanguageType()) {
            is KotlinEntry.DataClass -> {
                // determine if the data class is only a oneOf container
                val isOneOfContainer = node.children.none {
                    it.languageType[OutputLanguage.Kotlin] !is KotlinEntry.KotlinOneOf
                } && node.children.count {
                    it.languageType[OutputLanguage.Kotlin] is KotlinEntry.KotlinOneOf
                } == 1

                val isUnderRoot = tree.children.contains(node)

                if (isOneOfContainer) {
                    // tag DataClass as skip so we do not generate it
                    languageType.onlyGenerateNested = !isUnderRoot // only when not directly below root do we unnest
                    languageType.skipType = true
                } else {
                    // take out any messages in the children and attach them to the DataClass
                    val nestedTypes = node.children.filter {
                        when (it.kotlinLanguageTypeOrNull()) {
                            is KotlinEntry.DataClass -> true
                            is KotlinEntry.KotlinStringEnumeration -> true
                            is KotlinEntry.KotlinOneOf -> true
                            else -> false
                        }
                    }.toList()

                    if (nestedTypes.isNotEmpty()) {
                        logger.info { "Restructure nesting for ${node.nodeName} by moving its children " +
                                "${nestedTypes.map { it.nodeName }} into data class body" }
                        nestedTypes.map { it.kotlinLanguageType() }
                            .forEach {
                                when (it) {
                                    is KotlinEntry.DataClass -> it.onlyGenerateNested = true
                                    is KotlinEntry.KotlinStringEnumeration -> it.onlyGenerateNested = true
                                    is KotlinEntry.KotlinOneOf -> it.onlyGenerateNested = true
                                    else -> Unit
                                }
                            }
                        languageType.nestedTypes.addAll(nestedTypes)
                    }
                }
            }

            else -> {}
        }
    }

    /**
     * Determines the imports needed for a node.
     *
     * @return List of fully qualified imports
     */
    private fun determineImports(node: BaseNode): List<String> {
        val dependencies = mutableListOf<String>()
        node.children.forEach { child ->
            when (val languageType = child.languageType[OutputLanguage.Kotlin]) {
                is KotlinEntry.KotlinParameter -> {
                    // determine if this elsewhere or a sibling
                    val parameterType = languageType.kotlinType
                    val siblings = node.children - child
                    val isSiblingType = siblings.any { it.languageType[OutputLanguage.Kotlin] == parameterType }
                    val importPath = when (parameterType) {
                        is KotlinEntry.DataClass -> {
                            when (parameterType.packagePath.isNullOrBlank()) {
                                false -> parameterType.importPath()
                                true -> null
                            }
                        }

                        else -> null
                    }
                    if (!isSiblingType && !importPath.isNullOrBlank()) {
                        dependencies.add(importPath)
                    }
                }

                is KotlinEntry.KotlinOneOfParameter -> {
                    val importPath = when (val parameterType = languageType.kotlinType) {
                        is KotlinEntry.DataClass -> {
                            when (parameterType.packagePath.isNullOrBlank()) {
                                false -> parameterType.importPath()
                                true -> null
                            }
                        }

                        else -> null
                    }
                    if (!importPath.isNullOrBlank()) {
                        dependencies.add(importPath)
                    }
                }
            }
        }

        node.children.forEach {
            dependencies.addAll(determineImports(it))
        }

        return dependencies.distinct()
    }

    private fun filterOutIrrelevantBaseNodes(baseNodes: List<BaseNode>): List<BaseNode> {
        return baseNodes.filter {
            // language built-in types do not need to be generated
            it.nodeType !is NodeType.BuiltinType
        }.filterNot {
            // filter out any empty data classes, they violate kotlin and are useless
            it.kotlinIsEmptyDataClass().also { isEmpty ->
                if (isEmpty) {
                    logger.warn { "Found empty data class ${it.nodeName}, which cannot be translated to Kotlin" }
                }
            }
        }.filterNot {
            // check skipping flag, which is set, e.g. if a node is covered by episode data
            it.nodeType?.skipGenerating() ?: false

        }.filterNot {
            // do not attempt custom types to be generated if custom type generation is disabled
            (it.nodeType?.isCustomType() ?: false) && !generateCustomTypes
        }
    }

    override fun episodeData(node: BaseNode): EpisodeData {
        return when (val languageType = node.kotlinLanguageTypeOrNull()) {
            is KotlinEntry.DataClass -> EpisodeData.Model(
                languageType.packagePath ?: "",
                languageType.typeName
            )

            else -> throw Exception("No processable episode data for $node. Found: $languageType")
        }
    }

    private fun resolveClusteredType(node: BaseNode) {
        val cluster = node.clusteredTypes ?: return

        logger.info { "Received clustered type ${node.nodeName}, bulk handling ${cluster.joinToString { it.nodeName }} as well" }
        logger.info { "Cluster node types are ${cluster.joinToString { it.nodeType!!::class.simpleName!! }}" }
        if (node.kotlinClusterHandled()) {
            logger.info { "Cluster with ${node.nodeName} is already handled, skipping" }
            return
        }

        // generate all message language types so the references do not die; also enforce clustering in one file
        // order matters, this node is now enforcing naming rules, which is why it must be handled first
        val totalCluster = listOf(node) + cluster
        totalCluster.forEach { clusterNode ->
            when (val nodeType = clusterNode.nodeType) {
                is NodeType.Message -> {
                    logger.info { "Processing cluster node $clusterNode" }

                    // set filename for all of these to the same one, based on Node
                    // attach type node
                    clusterNode.kotlinLanguageType(
                        KotlinEntry.DataClass(
                            typeName = nodeType.kotlinEpisodeTypeName() ?: clusterNode.nodeName,
                            typeNameRaw = clusterNode.nodeName,
                            packagePath = nodeType.kotlinEpisodePackageName() ?: kotlinConfig.kotlinPackage
                        )
                    )
                }

                else -> throw Exception("$nodeType cannot be clustered, needs to be ${NodeType.Message::javaClass}")
            }
        }

        // mark cluster as resolved
        totalCluster.forEach { it.kotlinSetClusterHandled() }

        // no return or modified branching, we've handled the cluster and normal processing can continue
    }

    /**
     * Attaches a [KotlinEntry] to the given [BaseNode] and recursively calls the function on child nodes.
     *
     * @param node to attach proto data to.
     * @param parentNode ?
     */
    private fun attachKotlinEntryToBaseNode(node: BaseNode, parentNode: BaseNode?) {
        logger.info { "Processing $node" }

        resolveClusteredType(node)

        // attach kotlin entry based on node type
        when (val nodeType = node.nodeType) {
            is NodeType.Message -> setMessageLanguageType(node, nodeType)
            is NodeType.Parameter -> setParameterLanguageType(node, parentNode, nodeType)
            is NodeType.StringEnumeration -> setStringEnumerationLanguageType(node, nodeType)
            is NodeType.OneOf -> setOneOfLanguageType(node, nodeType)
            is NodeType.BuiltinType -> Unit // pass, no need to attach entries for built-in types
            else -> throw Exception("Handle unknown node type: ${node.nodeType}")
        }

        // perform node processing on all children
        node.children.forEach { attachKotlinEntryToBaseNode(it, node) }
    }

    private fun setMessageLanguageType(node: BaseNode, nodeType: NodeType.Message) {
        logger.debug("Process message node type: ${node.nodeType}")
        when (node.kotlinLanguageTypeOrNull()) {
            null -> node.kotlinLanguageType(
                KotlinEntry.DataClass(
                    typeName = nodeType.kotlinEpisodeTypeName() ?: node.nodeName,
                    typeNameRaw = node.nodeName,
                    packagePath = nodeType.kotlinEpisodePackageName() ?: kotlinConfig.kotlinPackage
                )
            )

            else -> logger.info("Node ${node.nodeName} already has language typed attached, skipping")
        }
    }

    private fun setParameterLanguageType(node: BaseNode, parentNode: BaseNode?, nodeType: NodeType.Parameter) {
        logger.debug("Process parameter node type ${node.nodeType} of ${nodeType.parentNode?.nodeName}")
        val parameterName = determineParameterName(node, nodeType)
        when (val parentLanguageType = parentNode?.kotlinLanguageTypeOrNull()) {
            is KotlinEntry.KotlinOneOf -> {
                val parameterType = nodeType.parameterType.parent!!.kotlinLanguageType() as KotlinEntry.DataClass
                node.kotlinLanguageType(
                    KotlinEntry.KotlinOneOfParameter(
                        parameterType,
                        parameterName,
                        parentLanguageType
                    )
                )
            }

            else -> {
                when (val parameterType = nodeType.parameterType.parent!!.kotlinLanguageTypeOrNull()) {
                    is KotlinEntry.KotlinStringEnumeration -> {
                        node.kotlinLanguageType(
                            KotlinEntry.KotlinParameter(
                                parameterType,
                                parameterName,
                                list = nodeType.list,
                                nullable = nodeType.optional
                            )
                        )
                    }

                    is KotlinEntry.DataClass -> {
                        node.kotlinLanguageType(
                            KotlinEntry.KotlinParameter(
                                parameterType,
                                parameterName,
                                list = nodeType.list,
                                nullable = nodeType.optional
                            )
                        )
                    }

                    else -> throw Exception("Unsupported language type: $parameterType")
                }
            }
        }
    }

    private fun setStringEnumerationLanguageType(node: BaseNode, nodeType: NodeType.StringEnumeration) {
        logger.debug("Process string enumeration node type: ${node.nodeType}")
        node.kotlinLanguageType(
            KotlinEntry.KotlinStringEnumeration(
                kotlinConfig.enumTypeName,
                nodeType.values.toList()
            )
        )
    }

    private fun setOneOfLanguageType(node: BaseNode, nodeType: NodeType.OneOf) {
        logger.debug("Process one-of node type: ${node.nodeType}")
        node.kotlinLanguageType(KotlinEntry.KotlinOneOf(nodeType.parent!!.nodeName))
    }

    companion object : Logging {
        fun parseConfig(config: String): KotlinConfig {
            return Toml.partiallyDecodeFromString(serializer(), config, KOTLIN_TOML_TABLE_NAME)
        }
    }
}

