package org.somda.protosdc_converter.converter.rust.base

import org.somda.protosdc_converter.converter.Proto3Generator.Companion.camelCaseToConstant
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.MOD_SEP
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.generateRust
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustBox
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustEnumDerive
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustEnumEnd
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustEnumField
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustEnumFieldProtoVariant
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustEnumFieldWithType
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustEnumProtoParams
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustEnumStart
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustModuleEnd
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustModuleName
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustModuleStart
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustOneOfDerive
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustOneOfProtoParams
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustParameterProtoParams
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustStructAllow
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustStructDerive
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustStructEnd
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustStructField
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustStructProtoParams
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustStructStart
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.rustVec
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.typeName
import org.somda.protosdc_converter.converter.rust.replacePrefixWithCrate
import org.somda.protosdc_converter.xmlprocessor.BaseLanguageType
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import java.io.ByteArrayOutputStream
import java.io.PrintWriter

/**
 * Class for modelling rust structs and converting them into actual code.
 */
sealed interface RustEntry : BaseLanguageType {

    val rustModule: String

    fun toRustEnter(level: Int = 0, nestedCall: Boolean = false): String

    fun toRustExit(level: Int = 0, nestedCall: Boolean = false): String

    fun toRustMain(level: Int = 0, nestedCall: Boolean = false): String

    data class RustStruct(
        val typeName: String,
        val builtinType: Boolean = false,
        var modulePath: String? = null,
        val wasAttribute: Boolean = false,
        var fileName: String? = null,
        private val _typeNameRaw: String? = null,
        val nestedTypes: MutableList<BaseNode> = mutableListOf(),
        var onlyGenerateNested: Boolean = false,
        var skipType: Boolean = false,
        override val rustModule: String,
        val protoType: String?,
        val customType: Boolean = false,
        val hasOptionalContent: Boolean = false,
    ) : RustEntry {

        override fun toRustEnter(level: Int, nestedCall: Boolean): String {
            // if this represents a simple type with a restriction that allows for defaults, also allow defaults
            return "${rustStructDerive(level = level, default = hasOptionalContent)}\n${protoType?.let { rustStructProtoParams(level, it) + "\n" } ?: ""}${rustStructStart(level = level, data = typeName)}"
        }

        override fun toRustExit(level: Int, nestedCall: Boolean): String {
            var returnValue = rustStructEnd(level = level)
            if (nestedTypes.isNotEmpty()) {
                returnValue += "\n"
                returnValue += rustModuleStart(level = level, data = rustModuleName(typeName))
                returnValue += "\n"

                val os = ByteArrayOutputStream()
                PrintWriter(os).use { printWriter ->
                    nestedTypes.forEach { nestedType ->
                        generateRust(nestedType, target = printWriter, level = level + 1, isNestedCall = true)
                    }
                }

                returnValue += os.toString()
                returnValue += rustModuleEnd(level = level)

            }
            return returnValue
        }

        // no "main" here, just the body
        override fun toRustMain(level: Int, nestedCall: Boolean): String = ""

        fun fullyQualifiedName(replacementPrefix: String? = null): String {
            val fqrn = "${modulePath?.let { it + MOD_SEP } ?: ""}$typeName"
            return replacementPrefix?.let { fqrn.replacePrefixWithCrate(replacementPrefix) } ?: fqrn
        }

    }

    /**
     * Enum with typed parameters, different from normal enum.
     */
    data class RustOneOfParameter(
        val rustType: RustEntry,
        val parameterName: String,
        val nestingLevel: Int = 0,
        var onlyGenerateNested: Boolean = false,
        val box: Boolean = false,
        override val rustModule: String,
        val primitive: Boolean,
        val targetIsCustomType: Boolean,
    ) : RustEntry {
        override fun toRustEnter(level: Int, nestedCall: Boolean): String = ""

        override fun toRustExit(level: Int, nestedCall: Boolean): String = ""

        override fun toRustMain(level: Int, nestedCall: Boolean): String {

            val annotation = rustParameterProtoParams(primitive = primitive,
                box = box,
                repeated = false,
                enumeration = null,
                optional = false,
                parameterName = null,
                targetIsCustomType = targetIsCustomType,
            )

            return rustEnumFieldWithType(
                level = level,
                annotation = annotation,
                name = parameterName,
                type = when (box) {
                    false -> typeName(rustType, nestedLevel = nestingLevel, rustModule = rustModule)
                    true -> rustBox(typeName(rustType, nestedLevel = nestingLevel, rustModule = rustModule))
                })
        }
    }

    data class RustParameter(
        val rustType: RustEntry,
        val parentType: RustEntry,
        val parameterName: String,
        val list: Boolean = false,
        val optional: Boolean = false,
        val nestingLevel: Int = 0,
        val box: Boolean = false,
        override val rustModule: String,
        val enumeration: String?,
        val protoParameterName: String?,
        val primitive: Boolean,
        val targetIsCustomType: Boolean,
    ) : RustEntry {
        override fun toRustEnter(level: Int, nestedCall: Boolean): String = ""

        override fun toRustExit(level: Int, nestedCall: Boolean): String = ""

        override fun toRustMain(level: Int, nestedCall: Boolean): String {

            // get parent type if we're nested
            val parentTypeModulePath: String = if (nestingLevel > 0) {
                when (val targetType = parentType) {
                    is RustStruct -> targetType.modulePath ?: targetType.rustModule
                    is RustStringEnumeration ->{
                        targetType.modulePath ?: targetType.rustModule
                    }
                    else -> TODO("Unknown target type $targetType")
                }
            } else {
                rustModule
            }

            val protoInfo = rustParameterProtoParams(primitive, box, list, enumeration, optional, protoParameterName, targetIsCustomType = targetIsCustomType)

            return when {
                list -> {
                    // there is no such thing as an optional (nullable) list, so these can be merged
                    val parameterType = rustVec(typeName(rustType, nestedLevel = nestingLevel, rustModule = parentTypeModulePath))
                    rustStructField(
                        level = level, annotation = protoInfo, parameterName = parameterName,
                        parameterType = parameterType, optional = false
                    )
                }
                else -> {
                    val parameterTypeName = when (box) {
                        false -> typeName(rustType, nestedLevel = nestingLevel, rustModule = parentTypeModulePath)
                        true -> rustBox(typeName(rustType, nestedLevel = nestingLevel, rustModule = parentTypeModulePath))
                    }

                    rustStructField(
                        level = level, annotation = protoInfo, parameterName = parameterName,
                        parameterType = parameterTypeName, optional = optional
                    )
                }
            }
        }
    }

    data class RustStringEnumeration(
        val enumName: String,
        val enumValues: List<String>,
        val modulePath: String?,
        var onlyGenerateNested: Boolean = false,
        override val rustModule: String,
        val protoType: String
    ) : RustEntry {
        override fun toRustEnter(level: Int, nestedCall: Boolean): String =
            "${rustStructAllow(level = level)}\n${rustEnumDerive(level = level)}\n${rustEnumProtoParams(level = level, protoType)}\n${rustEnumStart(level = level, data = enumName)}"

        override fun toRustExit(level: Int, nestedCall: Boolean): String = rustEnumEnd(level = level)

        override fun toRustMain(level: Int, nestedCall: Boolean): String {
            return enumValues.joinToString(separator = "\n") {
                rustEnumFieldProtoVariant(level = level, variant = it.camelCaseToConstant()) + "\n" + rustEnumField(level = level, name = it)
            }
        }

        fun fullyQualifiedName(replacementPrefix: String? = null): String {
            val fqrn = "${modulePath.let { it + MOD_SEP }}$enumName"
            return replacementPrefix?.let { fqrn.replacePrefixWithCrate(replacementPrefix) } ?: fqrn
        }
    }

    data class RustOneOf(
        val oneOfName: String,
        var onlyGenerateNested: Boolean = false,
        override val rustModule: String,
        var protoType: String,
        var protoFieldName: String,
        var protoModPath: String,
    ) : RustEntry {
        override fun toRustEnter(level: Int, nestedCall: Boolean): String =
            "${rustOneOfDerive(level = level)}\n${rustOneOfProtoParams(level = level, protoType, protoFieldName, protoModPath)}\n${rustEnumStart(level = level, data = oneOfName)}"

        override fun toRustExit(level: Int, nestedCall: Boolean): String = rustEnumEnd(level = level)

        override fun toRustMain(level: Int, nestedCall: Boolean): String = ""

        fun fullyQualifiedName(replacementPrefix: String? = null): String {
            val fqrn = "${rustModule.let { it + MOD_SEP }}$oneOfName"
            return replacementPrefix?.let { fqrn.replacePrefixWithCrate(replacementPrefix) } ?: fqrn
        }
    }

}