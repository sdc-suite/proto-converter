package org.somda.protosdc_converter.converter.kotlinmapper.proto

import org.somda.protosdc_converter.converter.kotlin.CodeLine
import org.somda.protosdc_converter.converter.kotlin.KotlinSource

/**
 * Utility functions to create various Kotlin code particles intended to be used by the Proto <-> Kotlin mapper.
 *
 * @param kotlinSource Kotlin code generator utility functions.
 * @param oneOfClassPrefix a prefix for data classes that represent a OneOf item, needed to create a separate type name
 *                         different from the enclosed data type of the OneOf item.
 * @param oneOfClassMemberName the type enclosed by a OneOf item gets this as a property name.
 */
class KotlinProtoMappingSource(
    private val kotlinSource: KotlinSource,
    private val oneOfClassPrefix: String,
    private val oneOfClassMemberName: String
) {
    fun kotlinFromProtoOneOfStart(protoTypeName: String): String {
        val caseName = caseName(KotlinSource.toParameterName(protoTypeName))
        return kotlinSource.returnAny(kotlinSource.whenStart("$SOURCE.$caseName"))
    }

    fun protoFromKotlinOneOfCase(
        kotlinChoice: String,
        protoCase: String,
        protoField: String,
        mappingFunction: String? = null
    ): String {
        val setterStart = "$protoCase.$NEW_BUILDER.set$protoField("
        val param = mappingCallOrField(mappingFunction, "$SOURCE.$oneOfClassMemberName")
        val setterEnd = ").$BUILD"
        return kotlinSource.whenEntry(
            condition = kotlinChoice,
            statement = setterStart + param + setterEnd
        )
    }

    fun mapFunctionStart(functionName: String, targetType: String, sourceType: String): String {
        return "fun $functionName($SOURCE: $sourceType): $targetType {"
    }

    fun mapFunctionName(functionName: String) = "map_$functionName"

    fun returnInstanceStart(targetType: String) = kotlinSource.returnAny("$targetType(")

    fun returnInstanceEnd() = ")"

    fun kotlinFromProtoField(
        kotlinField: String,
        protoField: String,
        mappingFunction: String? = null
    ): String {
        return kotlinSource.assignment(
            lhs = kotlinField,
            rhs = mappingCallOrField(mappingFunction, "$SOURCE.$protoField") + ","
        )
    }

    fun kotlinFromProtoList(
        kotlinField: String,
        protoField: String,
        mappingFunction: String? = null
    ): String {
        val mapperCall = mappingCallOrField(mappingFunction, "it")
        return kotlinSource.assignment(
            lhs = kotlinField,
            rhs = "$SOURCE.$protoField$LIST_SUFFIX.toList().map { $mapperCall }.toList(),"
        )
    }

    fun protoFromKotlinList(
        protoField: String,
        kotlinField: String,
        mappingFunction: String? = null
    ): String {
        val mapperCall = mappingCallOrField(mappingFunction, "it")
        return "$BUILDER.$ADD_ALL$protoField($SOURCE.$kotlinField.toList().map { $mapperCall }.toList())"
    }

    fun protoFromKotlinOneOfStart() = kotlinSource.returnAny(kotlinSource.whenStart(SOURCE))

    fun kotlinFromProtoOneOfCase(
        protoCase: String,
        protoField: String,
        kotlinChoice: String,
        mappingFunction: String? = null
    ) = kotlinSource.whenEntry(
        condition = protoCase,
        statement = mappingCallOrField(mappingFunction, "$SOURCE.$protoField").let { "$kotlinChoice($it)" }
    )

    fun kotlinFromOptionalProto(kotlinField: String, protoField: String) = kotlinSource.assignment(
        lhs = kotlinField,
        rhs = kotlinSource.whenStart("$SOURCE.has$protoField()")
    )

    fun kotlinFromOptionalProtoTrue(
        protoField: String,
        mappingFunction: String?
    ) = kotlinSource.whenEntry(
        condition = "true",
        statement = mappingCallOrField(mappingFunction, "$SOURCE.$protoField", protoField)
    )

    fun kotlinFromOptionalProtoFalse() = kotlinSource.whenEntry(
        condition = "false",
        statement = "null"
    )

    fun kotlinFromOptionalProtoEnd() = kotlinSource.blockEnd() + ","

    fun kotlinFromProtoEnumStart() = kotlinSource.returnAny(kotlinSource.whenStart(SOURCE))

    fun kotlinFromProtoEnumCase(
        sourceType: String,
        sourceValue: String,
        targetType: String,
        targetValue: String
    ) = kotlinSource.whenEntry(
        condition = "$sourceType.$sourceValue",
        statement = "$targetType.$targetValue"
    )

    fun kotlinFromProtoEnumElse() = kotlinSource.whenEntry(
        "else",
        """throw Exception("Unknown enum value $$SOURCE")"""
    )

    fun kotlinFromProtoEnumEnd() = kotlinSource.blockEnd()

    fun protoBuilderStart(builderName: String) =
        kotlinSource.valAssignment(BUILDER, "$builderName.$NEW_BUILDER")

    fun protoBuilderEnd() = kotlinSource.returnAny("$BUILDER.$BUILD")

    fun protoFieldFromOptionalKotlin(
        kotlinField: String,
        protoField: String,
        mappingFunction: String?
    ): String {
        val lambdaLiteral = "field"
        val letStart = "$SOURCE.$kotlinField?.let { $lambdaLiteral ->"
        val letStatement = kotlinSource.assignment(
            lhs = "$BUILDER.$protoField",
            rhs = mappingCallOrField(mappingFunction, lambdaLiteral)
        )
        val letEnd = "}"

        return kotlinSource.indentLines {
            listOf(
                CodeLine(letStart),
                CodeLine(1, letStatement),
                CodeLine(letEnd)
            )
        }
    }

    fun protoFieldFromKotlin(
        kotlinField: String,
        protoField: String,
        mappingFunction: String?
    ) = kotlinSource.assignment(
        lhs = "$BUILDER.$protoField",
        rhs = mappingCallOrField(mappingFunction, "$SOURCE.$kotlinField")
    )

    fun oneOfKotlinEntryName(name: String) = "$oneOfClassPrefix$name"

    companion object {
        // const for proto java generated code
        private const val CASE_SUFFIX = "Case"
        private const val BUILDER = "builder"
        private const val BUILD = "build()"
        private const val NEW_BUILDER = "newBuilder()"
        private const val LIST_SUFFIX = "List"
        private const val ADD_ALL = "addAll"

        // mapper source variable name
        private const val SOURCE = "source"

        /**
         * Creates a fully qualified name to be used in a mapper function name by replacing dot with underscores.
         */
        fun functionNameOfPackage(packageName: String) = packageName.replace(".", "_")

        fun functionNameOfNamespace(namespace: String) = namespace.map {
            if (it.isLetterOrDigit()) {
                it
            } else {
                '_'
            }
        }.joinToString("")

        /**
         * Resolves the given proto type name to the case field of that type by appending "Case"
         */
        fun caseName(protoTypeName: String) = "$protoTypeName$CASE_SUFFIX"

        /**
         * Generates a function call if function name is present, or passes throw a field.
         */
        private fun mappingCallOrField(funcName: String?, funcParam: String, field: String = funcParam): String {
            return funcName?.let { "$funcName($funcParam)" } ?: field
        }
    }
}