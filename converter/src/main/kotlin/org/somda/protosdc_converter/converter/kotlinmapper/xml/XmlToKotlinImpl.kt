package org.somda.protosdc_converter.converter.kotlinmapper.xml

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.kotlin.KotlinEntry
import org.somda.protosdc_converter.converter.kotlin.KotlinSource
import org.somda.protosdc_converter.converter.kotlin.kotlinLanguageType
import org.somda.protosdc_converter.converter.kotlin.typeName
import org.somda.protosdc_converter.converter.kotlinmapper.KotlinType
import org.somda.protosdc_converter.converter.kotlinmapper.proto.KotlinProtoMappingSource
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.PrintWriter
import javax.xml.namespace.QName

/**
 * Generates mapping functions for the mapping direction XML -> Kotlin.
 *
 * TODO: this class is a mess and needs to be cleaned up by removing redundant code segments
 */
internal class XmlToKotlinImpl(
    private val kotlinSource: KotlinSource,
    private val kotlinMappingSource: KotlinXmlMappingSource,
    private val util: KotlinXmlMapperUtil
) {
    private val xmlToKotlinMapperMap = util.xmlToKotlinMapperMap

    /**
     * Prints the mapping functions related to the given node to the given target print writer.
     *
     * @param node the root base node or currently processed node.
     * @param target the writer to which code will be generated.
     * @param level indentation offset for output.
     * @param parentNode parent node if present.
     */
    fun processNode(node: BaseNode, target: PrintWriter, level: Int = 0, parentNode: BaseNode? = null) {
        if (node.ignore) {
            return
        }

        node.clusteredTypes?.let { cluster ->
            if (cluster.isEmpty()) {
                return@let
            }

            logger.debug { "Encountered cluster ${node.clusteredTypes}" }

            if (node.xmlToKotlinClusterHandled()) {
                logger.debug { "Encountered cluster ${node.clusteredTypes} is already handled" }
                return@let
            }

            logger.debug { "Encountered cluster ${node.clusteredTypes} is new" }
            logger.debug { "Add cluster mappers all at once to map" }

            (listOf(node) + cluster).forEach { clusterType ->
                clusterType.xmlToKotlinSetClusterHandled()

                // create fully qualified types for mapping of clusterType
                val xmlName = util.fullyQualifiedXmlNameFor(clusterType)
                val kotlinName = util.fullyQualifiedKotlinNameFor(clusterType)

                // add mapping
                val functionName =
                    kotlinMappingSource.mapFunctionName(KotlinProtoMappingSource.functionNameOfPackage(kotlinName))
                xmlToKotlinMapperMap[xmlName, kotlinName] = functionName
            }
        }

        val kotlinLanguageType = node.kotlinLanguageType()
        val qualifiedXmlTypeName = util.fullyQualifiedXmlNameFor(node)
        val qualifiedKotlinTypeName = util.fullyQualifiedKotlinNameFor(node)
        val mapper = xmlToKotlinMapperMap[qualifiedXmlTypeName, qualifiedKotlinTypeName]

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {
                if (mapper != null && node.clusteredTypes == null) {
                    logger.debug {
                        "Skip generating mapper from non-clustered message $qualifiedXmlTypeName to " +
                                "$qualifiedKotlinTypeName, already present"
                    }
                    return
                }

                // separate parameter children from nested message children
                val parameters = node.children.filter { it.nodeType is NodeType.Parameter }.toSet()
                val nestedTypes = node.children - parameters

                val kotlinLanguageTypeSkip = when (kotlinLanguageType) {
                    is KotlinEntry.DataClass -> kotlinLanguageType.skipType
                    else -> false
                }

                // check if nested is only a oneof, this is a special case in kotlin
                val isOnlyOneOfWrapper = nestedTypes.all { it.nodeType is NodeType.OneOf }
                        && nestedTypes.isNotEmpty()
                        && kotlinLanguageTypeSkip
                val isOneOf = node.nodeType is NodeType.OneOf

                // we need to generate nested types first
                nestedTypes.forEach { child -> processNode(child, target, level, node) }

                if (isOnlyOneOfWrapper) {
                    // bail out if this was only a oneof, but create mapper from the msg to the nested type
                    return
                }

                // create mapper
                val kotlinName = util.fullyQualifiedKotlinNameFor(node)
                val functionName =
                    kotlinMappingSource.mapFunctionName(KotlinProtoMappingSource.functionNameOfPackage(kotlinName))

                xmlToKotlinMapperMap[qualifiedXmlTypeName, qualifiedKotlinTypeName] = functionName

                target.println(
                    kotlinSource.indent(level) {
                        kotlinMappingSource.xmlToKotlinFunctionStart(
                            functionName = functionName,
                            targetType = qualifiedKotlinTypeName,
                            sourceType = KotlinType.DOM_NODE
                        )
                    }
                )

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.returnInstanceStart(qualifiedKotlinTypeName)
                })

                // only for non oneOfs
                if (!isOneOf) {
                    parameters.forEach { child ->
                        processNode(child, target, level + 2, node)
                    }
                }

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.returnInstanceEnd()
                })
                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })
            }

            is NodeType.Parameter -> {
                when {
                    nodeType.list -> when (node.originalXmlNode?.xmlType) {
                        is XmlType.List -> {
                            target.println(kotlinSource.indent(level) {
                                kotlinMappingSource.xmlToKotlinList(
                                    util.getKotlinFieldName(kotlinLanguageType),
                                    mapper,
                                    TextNodeCreator.FUNCTION_NAME
                                )
                            })
                        }

                        else -> {
                            target.println(kotlinSource.indent(level) {
                                kotlinMappingSource.xmlSequenceToKotlinList(
                                    util.getKotlinFieldName(kotlinLanguageType),
                                    node.originalXmlNode!!.qName!!,
                                    mapper
                                )
                            })
                        }
                    }

                    nodeType.optional -> {
                        when (nodeType.wasAttribute) {
                            true -> {
                                target.println(kotlinSource.indent(level) {
                                    kotlinMappingSource.xmlAttributeToKotlin(
                                        util.getKotlinFieldName(kotlinLanguageType),
                                        node.nodeName,
                                    )
                                })
                                target.println(kotlinSource.indent(level + 1) {
                                    kotlinMappingSource.optionalXmlAttributeToKotlinNull()
                                })
                                target.println(kotlinSource.indent(level + 1) {
                                    kotlinMappingSource.xmlAttributeToKotlinElse(
                                        node.nodeName,
                                        mapper
                                    )
                                })
                                target.println(kotlinSource.indent(level) {
                                    kotlinMappingSource.xmlAttributeToKotlinEnd()
                                })
                            }

                            false -> {
                                target.println(kotlinSource.indentLines(level) {
                                    node.originalXmlNode!!.qName!!.let { qName ->
                                        kotlinMappingSource.xmlElementToKotlin(
                                            util.getKotlinFieldName(kotlinLanguageType),
                                            qName.namespaceURI,
                                            qName.localPart
                                        )
                                    }
                                })
                                target.println(kotlinSource.indent(level + 2) {
                                    kotlinMappingSource.optionalXmlElementToKotlinNull()
                                })
                                target.println(kotlinSource.indent(level + 2) {
                                    kotlinMappingSource.xmlElementToKotlinElse(
                                        mapper
                                    )
                                })
                                target.println(kotlinSource.indent(level) {
                                    kotlinMappingSource.xmlAttributeToKotlinEnd()
                                })
                            }
                        }
                    }

                    else -> {
                        when (node.originalXmlNode?.xmlType) {
                            is XmlType.AttributeGroupRef, is XmlType.AttributeGroup -> {
                                target.println(kotlinSource.indent(level) {
                                    kotlinMappingSource.xmlToKotlin(
                                        util.getKotlinFieldName(kotlinLanguageType),
                                        mapper
                                    )
                                })
                            }

                            is XmlType.Restriction, is XmlType.SimpleContentExtension -> when (nodeType.wasAttribute) {
                                true -> {
                                    target.println(kotlinSource.indent(level) {
                                        kotlinMappingSource.xmlAttributeToKotlin(
                                            util.getKotlinFieldName(kotlinLanguageType),
                                            node.nodeName,
                                        )
                                    })
                                    target.println(kotlinSource.indent(level + 1) {
                                        kotlinMappingSource.requiredXmlAttributeToKotlinNull(node.nodeName)
                                    })
                                    target.println(kotlinSource.indent(level + 1) {
                                        kotlinMappingSource.xmlAttributeToKotlinElse(
                                            node.nodeName,
                                            mapper
                                        )
                                    })
                                    target.println(kotlinSource.indent(level) {
                                        kotlinMappingSource.xmlAttributeToKotlinEnd()
                                    })
                                }

                                false -> {
                                    target.println(kotlinSource.indentLines(level) {
                                        kotlinMappingSource.xmlTextToKotlin(
                                            util.getKotlinFieldName(kotlinLanguageType),
                                            mapper
                                        )
                                    })
                                }
                            }

                            else -> {
                                when (nodeType.wasAttribute) {
                                    true -> {
                                        target.println(kotlinSource.indent(level) {
                                            kotlinMappingSource.xmlAttributeToKotlin(
                                                util.getKotlinFieldName(kotlinLanguageType),
                                                node.nodeName,
                                            )
                                        })
                                        target.println(kotlinSource.indent(level + 1) {
                                            kotlinMappingSource.requiredXmlAttributeToKotlinNull(node.nodeName)
                                        })
                                        target.println(kotlinSource.indent(level + 1) {
                                            kotlinMappingSource.xmlAttributeToKotlinElse(
                                                node.nodeName,
                                                mapper
                                            )
                                        })
                                        target.println(kotlinSource.indent(level) {
                                            kotlinMappingSource.xmlAttributeToKotlinEnd()
                                        })
                                    }

                                    false -> {
                                        when (nodeType.inheritance) {
                                            true -> {
                                                target.println(kotlinSource.indent(level) {
                                                    kotlinMappingSource.xmlToKotlin(
                                                        util.getKotlinFieldName(kotlinLanguageType),
                                                        mapper
                                                    )
                                                })
                                            }

                                            false -> {
                                                val parentType = parentNode?.originalXmlNode?.xmlType
                                                when (parentType is XmlType.Element && parentType.type != null) {
                                                    true  -> {
                                                        // is element with type ref, just call mapper function of type
                                                        target.println(kotlinSource.indent(level) {
                                                            kotlinSource.assignment(
                                                                util.getKotlinFieldName(kotlinLanguageType),
                                                                "$mapper(${KotlinXmlMappingSource.SOURCE}),"
                                                            )
                                                        })
                                                    }

                                                    false -> {
                                                        // is content/type/element w/o type ref; find element to map
                                                        target.println(kotlinSource.indentLines(level) {
                                                            node.originalXmlNode!!.qName!!.let { qName ->
                                                                kotlinMappingSource.xmlElementToKotlin(
                                                                    util.getKotlinFieldName(kotlinLanguageType),
                                                                    qName.namespaceURI,
                                                                    qName.localPart
                                                                )
                                                            }
                                                        })

                                                        target.println(kotlinSource.indent(level + 2) {
                                                            kotlinMappingSource.requiredXmlElementToKotlinNull(node.nodeName)
                                                        })

                                                        when (nodeType.parameterType) {
                                                            is NodeType.BuiltinType -> target.println(
                                                                kotlinSource.indentLines(
                                                                    level + 2
                                                                ) {
                                                                    kotlinMappingSource.xmlTextToKotlinElse(
                                                                        mapper
                                                                    )
                                                                })

                                                            else -> target.println(kotlinSource.indent(level + 2) {
                                                                kotlinMappingSource.xmlElementToKotlinElse(
                                                                    mapper
                                                                )
                                                            })
                                                        }

                                                        target.println(kotlinSource.indent(level) {
                                                            kotlinMappingSource.xmlAttributeToKotlinEnd()
                                                        })
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            is NodeType.StringEnumeration -> {
                // create mapper
                val kotlinName = util.fullyQualifiedKotlinNameFor(node)
                val functionName =
                    kotlinMappingSource.mapFunctionName(KotlinProtoMappingSource.functionNameOfPackage(kotlinName))

                xmlToKotlinMapperMap[qualifiedXmlTypeName, qualifiedKotlinTypeName] = functionName

                target.println(kotlinSource.indent(level) {
                    kotlinMappingSource.xmlToKotlinFunctionStart(
                        functionName = functionName,
                        targetType = qualifiedKotlinTypeName,
                        sourceType = KotlinType.DOM_NODE,
                    )
                })

                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.xmlToKotlinEnumStart()
                })

                nodeType.values.forEach { value ->
                    target.println(kotlinSource.indent(level + 2) {
                        kotlinMappingSource.xmlToKotlinEnumCase(
                            sourceValue = value,
                            targetType = qualifiedKotlinTypeName,
                            targetValue = value
                        )
                    })
                }
                target.println(kotlinSource.indent(level + 2) {
                    kotlinMappingSource.xmlToKotlinEnumElse()
                })
                target.println(kotlinSource.indent(level + 1) {
                    kotlinMappingSource.xmlToKotlinEnumEnd()
                })
                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })
            }

            is NodeType.BuiltinType -> {
                logger.debug { "Skip builtin type ${node.nodeName}" }
            }

            is NodeType.OneOf -> {
                // retrieve the fully qualified parent node name here for proto
                val oneOfXmlTypeName = when (node.originalXmlNode?.xmlType) {
                    is XmlType.Union -> util.fullyQualifiedXmlNameFor(parentNode!!)
                    else -> if (nodeType.defaultTypeParameter == null) {
                        util.fullyQualifiedXmlNameFor(parentNode!!)
                    } else {
                        util.fullyQualifiedXmlNameFor(nodeType.defaultTypeParameter!!) + "OneOf"
                    }
                }

                // create mapper
                val functionName = kotlinMappingSource.mapFunctionName(
                    KotlinXmlMappingSource.functionNameOfPackage(
                        qualifiedKotlinTypeName
                    )
                )

                xmlToKotlinMapperMap[oneOfXmlTypeName, qualifiedKotlinTypeName] = functionName

                target.println(kotlinSource.indent(level) {
                    kotlinMappingSource.xmlToKotlinFunctionStart(
                        functionName = functionName,
                        targetType = qualifiedKotlinTypeName,
                        sourceType = KotlinType.DOM_NODE,
                    )
                })

                when (node.originalXmlNode?.xmlType) {
                    is XmlType.Union -> {
                        for (child in node.children) {
                            check(child.nodeType is NodeType.Parameter)

                            val kotlinChild = child.kotlinLanguageType()

                            check(kotlinChild is KotlinEntry.KotlinOneOfParameter)

                            val childXmlTypeName = util.fullyQualifiedXmlNameFor(child)
                            val childKotlinTypeName = util.fullyQualifiedKotlinNameFor(child)

                            // generate oneof parameters
                            val childMapper = xmlToKotlinMapperMap[childXmlTypeName, childKotlinTypeName]

                            check(childMapper != null) {
                                "Cannot map from oneOf parameter $childXmlTypeName to $childKotlinTypeName, no mapper!"
                            }

                            val kotlinCaseName = KotlinSource.qualifiedPathFrom(
                                qualifiedKotlinTypeName,
                                kotlinMappingSource.oneOfKotlinEntryName(kotlinChild.kotlinType.typeName())
                            )

                            target.println(kotlinSource.indentLines(level + 1) {
                                kotlinMappingSource.xmlToKotlinUnionCase(
                                    kotlinChoice = kotlinCaseName,
                                    mappingFunction = childMapper
                                )
                            })
                        }
                    }

                    else -> {
                        when (val defaultNode = nodeType.defaultTypeParameter) {
                            // no default type parameter => must be choice (which do not have a distinctive XML Schema type)
                            null -> {
                                for (child in node.children) {
                                    check(child.nodeType is NodeType.Parameter)

                                    val kotlinChild = child.kotlinLanguageType()

                                    check(kotlinChild is KotlinEntry.KotlinOneOfParameter)

                                    val childXmlTypeName = util.fullyQualifiedXmlNameFor(child)
                                    val childKotlinTypeName = util.fullyQualifiedKotlinNameFor(child)

                                    // generate oneof parameters
                                    val childMapper = xmlToKotlinMapperMap[childXmlTypeName, childKotlinTypeName]

                                    check(childMapper != null) {
                                        "Cannot map from oneOf parameter $childXmlTypeName to $childKotlinTypeName, no mapper!"
                                    }

                                    val kotlinCaseName = KotlinSource.qualifiedPathFrom(
                                        qualifiedKotlinTypeName,
                                        kotlinMappingSource.oneOfKotlinEntryName(kotlinChild.kotlinType.typeName())
                                    )

                                    target.println(kotlinSource.indentLines(level + 1) {
                                        kotlinMappingSource.xmlToKotlinUnionCase(
                                            kotlinChoice = kotlinCaseName,
                                            mappingFunction = childMapper
                                        )
                                    })
                                }
                            }

                            // default type parameter => must be inheritance one-of
                            else -> {
                                val defaultKotlinEntry = defaultNode.kotlinLanguageType()
                                check(defaultKotlinEntry is KotlinEntry.KotlinOneOfParameter)

                                val defaultXmlTypeName = util.fullyQualifiedXmlNameFor(defaultNode)
                                val defaultKotlinTypeName = util.fullyQualifiedKotlinNameFor(defaultNode)
                                val defaultMapper = xmlToKotlinMapperMap[defaultXmlTypeName, defaultKotlinTypeName]

                                checkNotNull(defaultMapper != null) {
                                    "Cannot map from default oneOf parameter $defaultXmlTypeName to $defaultKotlinTypeName, no mapper!"
                                }

                                val defaultKotlinCaseName = KotlinSource.qualifiedPathFrom(
                                    qualifiedKotlinTypeName,
                                    kotlinMappingSource.oneOfKotlinEntryName(defaultKotlinEntry.kotlinType.typeName())
                                )

                                target.println(kotlinSource.indentLines(level + 1) {
                                    kotlinMappingSource.xmlInheritanceToKotlinStart(
                                        xmlQName = QName.valueOf(defaultXmlTypeName),
                                        kotlinChoice = defaultKotlinCaseName,
                                        mappingFunction = defaultMapper
                                    )
                                })

                                node.children.forEach { child ->
                                    check(child.nodeType is NodeType.Parameter)

                                    val childKotlinEntry = child.kotlinLanguageType()

                                    check(childKotlinEntry is KotlinEntry.KotlinOneOfParameter)

                                    val childXmlTypeName = util.fullyQualifiedXmlNameFor(child)
                                    val childKotlinTypeName = util.fullyQualifiedKotlinNameFor(child)

                                    // generate oneof parameters
                                    val childMapper = xmlToKotlinMapperMap[childXmlTypeName, childKotlinTypeName]

                                    check(childMapper != null) {
                                        "Cannot map from oneOf parameter $childXmlTypeName to $childKotlinTypeName, no mapper!"
                                    }

                                    val kotlinCaseName = KotlinSource.qualifiedPathFrom(
                                        qualifiedKotlinTypeName,
                                        kotlinMappingSource.oneOfKotlinEntryName(childKotlinEntry.kotlinType.typeName())
                                    )

                                    target.println(kotlinSource.indent(level + 3) {
                                        kotlinMappingSource.xmlInheritanceToKotlinEntry(
                                            xmlQName = QName.valueOf(childXmlTypeName),
                                            kotlinChoice = kotlinCaseName,
                                            mappingFunction = childMapper
                                        )
                                    })
                                }

                                target.println(kotlinSource.indent(level + 2) {
                                    kotlinSource.blockEnd()
                                })
                                target.println(kotlinSource.indent(level + 1) {
                                    kotlinSource.blockEnd()
                                })
                            }
                        }
                    }
                }

                target.println(kotlinSource.indent(level + 1) {
                    """throw ${KotlinType.EXCEPTION}("Mapping of $oneOfXmlTypeName to $qualifiedKotlinTypeName failed")"""
                })
                target.println(kotlinSource.indent(level) {
                    kotlinSource.blockEnd()
                })
            }

            else -> throw Exception("Cannot handle nodeType ${node.nodeType}")
        }
    }

    private companion object : Logging
}
