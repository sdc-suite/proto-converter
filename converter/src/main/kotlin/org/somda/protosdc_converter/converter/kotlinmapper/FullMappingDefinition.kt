package org.somda.protosdc_converter.converter.kotlinmapper

import org.somda.protosdc_converter.converter.kotlin.CodeLine

/**
 * Mapper metadata including mapping function definition.
 *
 * @param sourceType source type for the mapping.
 * @param targetType target type for the mapping.
 * @param mapperFunctionName the function name that performs the mapping.
 * @param code the mapping function definition.
 */
data class FullMappingDefinition(
    val sourceType: String,
    val targetType: String,
    val mapperFunctionName: MapperFunctionName,
    val code: List<CodeLine>
)