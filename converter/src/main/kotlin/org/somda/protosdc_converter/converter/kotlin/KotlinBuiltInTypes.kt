package org.somda.protosdc_converter.converter.kotlin

import org.somda.protosdc_converter.xmlprocessor.BuiltinTypes
import javax.xml.namespace.QName

/**
 * Access to Kotlin type maps.
 *
 * Type maps can be used for built-in types representation or custom type maps.
 */
interface KotlinTypeMap {
    /**
     * Retrieves a map with all types represented by this instance.
     */
    operator fun invoke(): Map<QName, KotlinEntry.DataClass>

    /**
     * Looks up a specific type based on a [QName].
     */
    operator fun get(type: QName): KotlinEntry.DataClass?
}

/**
 * Kotlin built-in types.
 */
object KotlinBuiltInTypes: KotlinTypeMap {
    private const val JAVA_TIME_PACKAGE = "java.time"

    private val types = mapOf(
        BuiltinTypes.XSD_STRING.qname to KotlinEntry.DataClass("String"),
        BuiltinTypes.XSD_DECIMAL.qname to KotlinEntry.DataClass("BigDecimal", "java.math"),
        BuiltinTypes.XSD_INTEGER.qname to KotlinEntry.DataClass("Long"),
        BuiltinTypes.XSD_ULONG.qname to KotlinEntry.DataClass("Long"),
        BuiltinTypes.XSD_BOOL.qname to KotlinEntry.DataClass("Boolean"),
        BuiltinTypes.XSD_DATE.qname to KotlinEntry.DataClass("LocalDate", JAVA_TIME_PACKAGE),
        BuiltinTypes.XSD_DATE_TIME.qname to KotlinEntry.DataClass("LocalDateTime", JAVA_TIME_PACKAGE),
        BuiltinTypes.XSD_G_YEAR_MONTH.qname to KotlinEntry.DataClass("YearMonth", JAVA_TIME_PACKAGE),
        BuiltinTypes.XSD_G_YEAR.qname to KotlinEntry.DataClass("Year", JAVA_TIME_PACKAGE),
        BuiltinTypes.XSD_QNAME.qname to KotlinEntry.DataClass("QName", "javax.xml.namespace"),
        BuiltinTypes.XSD_LANGUAGE.qname to KotlinEntry.DataClass("String"),
        BuiltinTypes.XSD_LONG.qname to KotlinEntry.DataClass("Long"),
        BuiltinTypes.XSD_UINT.qname to KotlinEntry.DataClass("Int"),
        // java.net.URI type causes trouble, hence go for simple string
        BuiltinTypes.XSD_ANY_URI.qname to KotlinEntry.DataClass("String"),
        BuiltinTypes.XSD_INT.qname to KotlinEntry.DataClass("Int"),
        BuiltinTypes.XSD_ANY_SIMPLE_TYPE.qname to KotlinEntry.DataClass("String"),
        BuiltinTypes.XSD_DURATION.qname to KotlinEntry.DataClass("Duration", "kotlin.time"),
        BuiltinTypes.XSD_ANY.qname to KotlinEntry.DataClass("Any"),
    )

    override fun invoke(): Map<QName, KotlinEntry.DataClass> = types

    override operator fun get(type: QName): KotlinEntry.DataClass? = types[type]
}