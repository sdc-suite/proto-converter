package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.QuickXmlParserConfig
import org.somda.protosdc_converter.xmlprocessor.BaseNode

/**
 * Interface implemented by parser generators, regardless of parser type to generate.
 */
interface ParserTracker {
    fun generateParser(
        config: QuickXmlParserConfig,
        primitiveParsers: MutableMap<BaseNode, RustParser>,
        simpleParsers: MutableMap<BaseNode, RustParser>,
        complexParsers: MutableMap<BaseNode, RustParser>
    ): String
}

typealias RustParser = String // just to indicate this must be a fully qualified name