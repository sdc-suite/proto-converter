package org.somda.protosdc_converter.converter.kotlinmapper

import org.somda.protosdc_converter.converter.kotlin.KotlinSource

/**
 * The (fully qualified) function name that maps from a source (param value) to a target (return value).
 */
typealias MapperFunctionName = String

/**
 * Access to mapper metadata `(source type, target type) => function name`.
 */
class MapperMetadataRegistry {
    private val data: MutableMap<MappingTypesNames, MapperFunctionName> = mutableMapOf()

    /**
     * Resolves the function name for a given source and target type of this registry.
     */
    operator fun get(sourceType: String, targetType: String): MapperFunctionName? =
        data[MappingTypesNames(sourceType, targetType)]

    /**
     * Sets a function name for a given source and target type of this registry.
     */
    operator fun set(sourceTypeName: String, targetTypeName: String, funcName: MapperFunctionName) {
        data[MappingTypesNames(sourceTypeName, targetTypeName)] = funcName
    }

    private data class MappingTypesNames(val sourceType: String, val targetType: String)
}

/**
 * Creates a mapper function name by concatenating the package particles.
 *
 * @param basePackageName the package prefix path
 * @param childPackageName child package name to the `basePackageName` (another package or class name)
 * @param functionName the mapper function name.
 */
fun mapperFunctionName(
    basePackageName: String,
    childPackageName: String,
    functionName: String
): MapperFunctionName {
    return KotlinSource.qualifiedPathFrom(basePackageName, childPackageName, functionName)
}