package org.somda.protosdc_converter.converter.rust.xml

import org.somda.protosdc_converter.converter.rust.PARSER_ERROR
import org.somda.protosdc_converter.converter.rust.QuickXmlParserConfig
import org.somda.protosdc_converter.converter.rust.RustGenerator.Companion.INDENT
import org.somda.protosdc_converter.converter.rust.base.RustEntry
import org.somda.protosdc_converter.converter.rust.primitiveParserCall
import org.somda.protosdc_converter.converter.rust.simpleParserCall
import org.somda.protosdc_converter.converter.rust.simpleParserSignature
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage

data class SimpleTypeParser(
    val parserName: String,
    val dataType: BaseNode,
    val rustModulePath: String,
    val primitiveParser: String?,
    val simpleParser: String?,
) : ParserTracker {

    init {
        check((simpleParser != null).xor(primitiveParser != null))
    }

    override fun generateParser(
        config: QuickXmlParserConfig,
        primitiveParsers: MutableMap<BaseNode, RustParser>,
        simpleParsers: MutableMap<BaseNode, RustParser>,
        complexParsers: MutableMap<BaseNode, RustParser>
    ): String {
        check(dataType.children.size == 1)

        val dataTypeRustStruct = dataType.languageType[OutputLanguage.Rust]!! as RustEntry.RustStruct

        val parameter = dataType.children[0]
        val rustParameter = parameter.languageType[OutputLanguage.Rust]!! as RustEntry.RustParameter
        val rustParameterTargetType =
            (parameter.nodeType as NodeType.Parameter).parameterType.parent!!.languageType[OutputLanguage.Rust] as RustEntry.RustStruct

        val fullyQualifiedRustTypeName = dataTypeRustStruct.fullyQualifiedName(config.modelRustModuleSubstitute)
        val fullyQualifiedRustTargetTypeName =
            rustParameterTargetType.fullyQualifiedName(config.modelRustModuleSubstitute)

        val valueVariable = "parsed"

        val parserCall: (String) -> (String) = when {
            primitiveParser != null -> { variable -> primitiveParserCall(primitiveParser, variable) }
            simpleParser != null -> { variable -> simpleParserCall(fullyQualifiedRustTargetTypeName, variable) }
            else -> TODO("Parser for $this")
        }


        val assignmentCall = when {
            !rustParameter.list && !rustParameter.optional -> "${INDENT.repeat(2)}let $valueVariable = ${
                parserCall(
                    "data"
                )
            }?;"

            !rustParameter.list && rustParameter.optional -> "${INDENT.repeat(2)}let $valueVariable = Some(${
                parserCall(
                    "data"
                )
            }?);"

            rustParameter.list && !rustParameter.optional -> {
                """${INDENT.repeat(2)}let elements = data.split(|it| it == &32u8); // split on space
${INDENT.repeat(2)}let $valueVariable = elements.map(|it| ${parserCall("it")}).collect::<Result<Vec<_>, $PARSER_ERROR>>()?;"""
            }

            else -> TODO("$rustParameter")
        }

        return """${simpleParserSignature(fullyQualifiedRustTypeName, config.extensionAssociatedType)}
$assignmentCall
        Ok(Self {
            ${rustParameter.parameterName}: $valueVariable
        })
    }
}"""
    }
}

