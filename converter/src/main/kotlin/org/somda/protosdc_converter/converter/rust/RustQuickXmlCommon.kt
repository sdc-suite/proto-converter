package org.somda.protosdc_converter.converter.rust

import org.somda.protosdc_converter.converter.rust.xml.ChildCollection
import org.somda.protosdc_converter.converter.rust.xml.ParserElementInfo
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.XmlType


fun filterTreeElements(tree: BaseNode, generateCustomTypes: Boolean): List<BaseNode> {
    return tree.children
        .filter { it.nodeType !is NodeType.BuiltinType }
        .filter {
            when (val nodeType = it.nodeType) {
                is NodeType.Message -> !nodeType.customType || generateCustomTypes
                else -> true
            }
        }
        .filter {
            when (val nodeType = it.nodeType) {
                is NodeType.Message -> !nodeType.skipGenerating
                else -> true
            }
        }
}

fun getElementType(node: BaseNode): BaseNode? {
    val typeOnly = node.originalXmlNode?.let { originalNode ->
        when (val nodeElement = originalNode.xmlType) {
            is XmlType.Element -> {
                nodeElement.type != null
            }

            else -> false
        }
    } ?: false

    if (!typeOnly || node.children.size != 1 || node.children[0].nodeType !is NodeType.Parameter) {
        return null
    }

    return (node.children[0].nodeType as NodeType.Parameter).parameterType.parent
}

/**
 * Determines if this node has content, i.e. if it extends a simple type
 *
 * @return Pair in which first Element is the BaseNode with NodeType.Parameter and the second is the type referenced by NodeType.Parameter
 */
fun findContentTypeNode(node: BaseNode): Pair<BaseNode, BaseNode>? {
    // since we're skipping anonymous complex types, we might encounter an element with simple content extension
    return if (node.originalXmlNode?.xmlType is XmlType.ComplexType || node.originalXmlNode?.xmlType is XmlType.Element) {
        val extensionChild = node.children.find { it.originalXmlNode?.xmlType is XmlType.SimpleContentExtension }
        (extensionChild?.nodeType as? NodeType.Parameter)?.parameterType?.parent?.let {
            Pair(extensionChild, it)
        }
    } else {
        null
    }
}

/**
 * Determines whether the given node is a parameter for text content of an XML tag.
 */
fun isContentParameter(node: BaseNode): Boolean {
    return when (val nodeType = node.nodeType) {
        is NodeType.Parameter -> {
            when (nodeType.wasAttribute) {
                false -> {
                    node.originalXmlNode?.xmlType == XmlType.SimpleType || (nodeType.inheritance && nodeType.parameterType.parent?.originalXmlNode?.xmlType == XmlType.SimpleType)
                        || node.originalXmlNode?.xmlType is XmlType.SimpleContentExtension
                }

                true -> false
            }
        }

        else -> throw IllegalArgumentException("Not parameter")
    }
}


/**
 * Returns true if the node is a container for a Choice
 **/
fun isChoiceContainer(node: ParserElementInfo): Boolean =
    node.children?.size == 1 && node.children[0] is ChildCollection.Choice

/**
 * Returns true if the node is a container for a Choice
 **/
fun isChoiceContainer(node: BaseNode): Boolean =
    node.children.size == 1 && node.children[0].nodeType is NodeType.OneOf && node.children[0].originalXmlNode?.xmlType == XmlType.Choice

/**
 * Returns true if the node is a container for an Enum, i.e. it only contains an enumeration and a parameter
 * pointing to it.
 **/
fun isEnumContainer(node: BaseNode) =
    node.children.size == 2 && node.children[0].nodeType is NodeType.StringEnumeration && node.children[1].nodeType is NodeType.Parameter

/**
 * Returns true if the node is a container for a union, i.e. it only contains OneOf node derived from an xml union.
 **/
fun isUnionContainer(node: BaseNode) =
    node.children.size == 1 && node.children[0].nodeType is NodeType.OneOf && node.children[0].originalXmlNode?.xmlType == XmlType.Union

fun isUnionContainerType(element: BaseNode) =
    element.nodeType is NodeType.Message && element.children.size == 1 && element.children[0].originalXmlNode?.xmlType == XmlType.Union
