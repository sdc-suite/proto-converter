package org.somda.protosdc_converter.converter.kotlinmapper.proto

import com.akuleshov7.ktoml.Toml
import kotlinx.serialization.serializer
import org.somda.protosdc_converter.converter.BaseNodePlugin
import org.somda.protosdc_converter.converter.ConverterConfig
import org.somda.protosdc_converter.converter.kotlin.CodeLine
import org.somda.protosdc_converter.converter.kotlin.KotlinConfig
import org.somda.protosdc_converter.converter.kotlin.KotlinSource
import org.somda.protosdc_converter.converter.kotlin.appendKotlinFileExtension
import org.somda.protosdc_converter.converter.kotlin.determineFilesToWrite
import org.somda.protosdc_converter.converter.kotlin.setupOutputFolders
import org.somda.protosdc_converter.converter.kotlin.writeByteStreamToFile
import org.somda.protosdc_converter.converter.kotlinmapper.KotlinType
import org.somda.protosdc_converter.converter.kotlinmapper.findBicepsExtensions
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.EpisodeData
import java.io.ByteArrayOutputStream
import java.io.PrintWriter
import javax.xml.namespace.QName

const val KOTLIN_PROTO_BICEPS_EXTENSION_TOML_TABLE_NAME = "KotlinProtoExtensions"

/**
 * Generates object class that maps known extensions from Proto to Kotlin and vice versa.
 */
class KotlinProtoExtensionsPlugin(
    tree: BaseNode,
    converterConfig: ConverterConfig,
    kotlinConfig: KotlinConfig,
    private val kotlinProtoMappingConfig: KotlinProtoMappingConfig,
    private val kotlinExtensionsConfig: KotlinProtoExtensionsConfig,
    customExtensionNodes: List<BaseNode>,
    private val mapperUtil: KotlinProtoMapperUtil
) : BaseNodePlugin {
    private val kotlinSource = KotlinSource(
        kotlinConfig.indentationSize.toInt(),
        kotlinConfig.oneOfClassPrefix,
        kotlinConfig.oneOfClassMemberName
    )

    private val allExtensionNodes = tree.findBicepsExtensions() + customExtensionNodes

    private val outputFolders = setupOutputFolders(
        converterConfig.outputFolder,
        kotlinExtensionsConfig.outputFolders,
        kotlinExtensionsConfig.outputFoldersNoHierarchy
    ) { "Output folders are missing for Kotlin BICEPS Extensions Handler generator" }

    override fun run() {
        // no namespace, no mapper to write
        val namespace = resolveNamespace()?.namespaceURI ?: return

        // determine directory structure required based on package
        val filesToWrite = determineFilesToWrite(
            outputFolders,
            kotlinExtensionsConfig.mappingPackage,
            appendKotlinFileExtension(kotlinExtensionsConfig.outputClassName)
        )

        val byteStream = ByteArrayOutputStream()
        PrintWriter(byteStream).use { printWriter ->
            // print package declaration
            printWriter.println(kotlinSource.packageDeclaration(kotlinExtensionsConfig.mappingPackage))
            printWriter.println()

            // no imports needed, every type is rendered fully qualified

            // begin object class, both mapping directions are included
            printWriter.println(
                when (val ifName = kotlinExtensionsConfig.fullyQualifiedInterfaceName) {
                    null -> kotlinSource.objectStart(kotlinExtensionsConfig.outputClassName)
                    else -> kotlinSource.objectStart("${kotlinExtensionsConfig.outputClassName} : $ifName")
                }
            )

            printWriter.println(kotlinSource.indent(1) {
                prependOverride("""fun namespace() = "$namespace"""")
            })
            printWriter.println()

            // generate mapping direction Proto -> Kotlin
            writeProtoToKotlinFunctions(printWriter)

            // generate mapping direction Kotlin -> Proto
            writeKotlinToProtoFunctions(printWriter)

            printWriter.println(kotlinSource.blockEnd())
        }

        writeByteStreamToFile(byteStream, filesToWrite)
    }

    private fun writeProtoToKotlinFunctions(target: PrintWriter) {
        target.println(kotlinSource.indent(1) {
            prependOverride(
                mapperFunctionStart(
                    MAP_TO_KOTLIN,
                    KotlinType.PROTOBUF_ANY,
                    KotlinType.nullable(KotlinType.KOTLIN_ANY),
                )
            )
        })

        // loop all extensions
        for (node in allExtensionNodes) {
            val protoType = mapperUtil.protoToKotlinType(mapperUtil.fullyQualifiedProtoNameFor(node))
            val kotlinType = mapperUtil.fullyQualifiedKotlinNameFor(node)
            val extensionMapperFunction = KotlinSource.qualifiedPathFrom(
                kotlinProtoMappingConfig.mappingPackage,
                kotlinProtoMappingConfig.protoToKotlinClassName,
                mapperUtil.protoToKotlinMapperMap[protoType, kotlinType]!!
            )

            target.println(kotlinSource.indentLines(2) {
                listOf(
                    CodeLine(
                        indentOffset = 0,
                        line = kotlinSource.ifStart(
                            "$PARAM_NAME.typeUrl.startsWith(namespace()) && " +
                                    "$PARAM_NAME.`is`($protoType::class.java)"
                        )
                    ),
                    CodeLine(
                        indentOffset = 1,
                        line = kotlinSource.returnAny(
                            "$extensionMapperFunction($PARAM_NAME.unpack($protoType::class.java))"
                        )
                    ),
                    CodeLine(
                        indentOffset = 0,
                        line = kotlinSource.blockEnd()
                    )
                )
            })
        }

        // callback for else branch
        target.println(kotlinSource.indent(2) {
            kotlinSource.returnAny("null")
        })

        // close blocks
        target.println(kotlinSource.indent(1) {
            kotlinSource.blockEnd()
        })

        target.println()
    }

    private fun writeKotlinToProtoFunctions(target: PrintWriter) {
        target.println(kotlinSource.indent(1) {
            prependOverride(
                mapperFunctionStart(
                    MAP_TO_PROTO,
                    KotlinType.KOTLIN_ANY,
                    KotlinType.nullable(KotlinType.PROTOBUF_ANY)
                )
            )
        })
        target.println(kotlinSource.indent(2) {
            kotlinSource.returnAny(kotlinSource.whenStart(PARAM_NAME))
        })

        // loop all extensions
        for (node in allExtensionNodes) {
            val protoType = mapperUtil.protoToKotlinType(mapperUtil.fullyQualifiedProtoNameFor(node))
            val kotlinType = mapperUtil.fullyQualifiedKotlinNameFor(node)
            target.println(kotlinSource.indent(3) {
                // map two known types
                val extensionMapperFunction = KotlinSource.qualifiedPathFrom(
                    kotlinProtoMappingConfig.mappingPackage,
                    kotlinProtoMappingConfig.kotlinToProtoClassName,
                    mapperUtil.kotlinToProtoMapperMap[kotlinType, protoType]!!
                )

                // this is an official extension since a mustUnderstand attribute is available
                // create straightforward mapping
                kotlinSource.whenEntry(
                    condition = "is $kotlinType",
                    statement = "${KotlinType.PROTOBUF_ANY}.pack($extensionMapperFunction($PARAM_NAME), namespace())"
                )

            })
        }

        // callback for else branch
        target.println(kotlinSource.indent(3) {
            kotlinSource.whenEntry(
                condition = "else",
                statement = "null"
            )
        })

        // close blocks
        repeat(2) {
            target.println(kotlinSource.indent(2 - it) {
                kotlinSource.blockEnd()
            })
        }
        target.println()
    }

    override fun episodeData(node: BaseNode): EpisodeData {
        throw Exception("${this.javaClass} does not provide ${EpisodeData::class.java}")
    }

    override fun pluginIdentifier() = "kotlinprotobicepsextensionsv1"

    private fun mapperFunctionStart(functionName: String, sourceType: String, targetType: String) =
        "fun $functionName($PARAM_NAME: $sourceType): $targetType {"

    private fun prependOverride(line: String) = when (kotlinExtensionsConfig.fullyQualifiedInterfaceName) {
        null -> line
        else -> "override $line"
    }

    private fun resolveNamespace(): QName? {
        val qName = allExtensionNodes.firstOrNull()?.let { firstNode ->
            firstNode.originalXmlNode?.qName!!
        } ?: return null

        require(allExtensionNodes.filterNot { it.originalXmlNode?.qName?.namespaceURI == qName.namespaceURI }
            .isEmpty()) {
            "All extensions need to share the same namespace."
        }

        return qName
    }

    companion object {
        private const val PARAM_NAME = "data"
        private const val MAP_TO_KOTLIN = "mapKnownToKotlin"
        private const val MAP_TO_PROTO = "mapKnownToProto"

        fun parseConfig(config: String): KotlinProtoExtensionsConfig {
            return Toml.partiallyDecodeFromString(serializer(), config, KOTLIN_PROTO_BICEPS_EXTENSION_TOML_TABLE_NAME)
        }
    }
}
