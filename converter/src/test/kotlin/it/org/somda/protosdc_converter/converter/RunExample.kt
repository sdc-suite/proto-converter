package it.org.somda.protosdc_converter.converter

import org.junit.jupiter.api.Test
import org.somda.protosdc_converter.converter.Main
import java.io.File

class RunExample {

    @Test
    fun runBase() {
        Main().main(arrayOf(
            "-c", "../config.toml.example"
        ))
    }

    @Test
    fun runBaseAndEpisode() {
        Main().main(arrayOf(
            "-c", "../config.toml.example"
        ))

        Main().main(arrayOf(
            "-c", "../config.toml.example.episode"
        ))
    }

}