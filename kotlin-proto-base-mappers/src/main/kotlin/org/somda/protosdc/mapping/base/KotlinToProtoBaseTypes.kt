package org.somda.protosdc.mapping.base

import com.google.protobuf.*
import com.google.protobuf.Any
import org.apache.logging.log4j.kotlin.Logging
import java.math.BigDecimal
import java.net.URI
import java.time.*
import kotlin.time.Duration
import javax.xml.namespace.QName
import kotlin.time.Duration.Companion.nanoseconds

typealias KotlinToProtoAnyHandler = (value: kotlin.Any) -> Any

@Suppress("unused", "MemberVisibilityCanBePrivate")
object KotlinToProtoBaseTypes : Logging {
    private var anyHandler: KotlinToProtoAnyHandler? = null

    fun mapURI(uri: URI): String {
        return uri.toString()
    }

    fun mapURIOptional(uri: URI): StringValue {
        return mapToStringValue(mapURI(uri))
    }

    fun mapToUInt32(value: Int): UInt32Value {
        return UInt32Value.of(value)
    }

    fun mapToUInt64(value: Long): UInt64Value {
        return UInt64Value.of(value)
    }

    fun mapToInt32Value(value: Int): Int32Value {
        return Int32Value.of(value)
    }

    fun mapToInt64Value(value: Long): Int64Value {
        return Int64Value.of(value)
    }

    fun mapToStringValue(value: String): StringValue {
        return StringValue.of(value)
    }

    fun mapToBoolValue(value: Boolean): BoolValue {
        return BoolValue.of(value)
    }

    fun mapBigDecimal(value: BigDecimal): String {
        return value.toPlainString()
    }

    fun mapBigDecimalOptional(value: BigDecimal): StringValue {
        return mapToStringValue(mapBigDecimal(value))
    }

    fun mapQName(value: QName): String {
        return value.toString()
    }

    fun mapQNameOptional(value: QName): StringValue {
        return mapToStringValue(mapQName(value))
    }

    fun mapDuration(value: Duration): com.google.protobuf.Duration {
        val inNanos = value.inWholeNanoseconds
        val inSeconds = value.inWholeSeconds
        val secondsInNanos = (inSeconds * 1_000_000_000).nanoseconds.inWholeNanoseconds
        val nanosFraction = inNanos - secondsInNanos
        return com.google.protobuf.Duration.newBuilder()
            .setSeconds(inSeconds)
            .setNanos(nanosFraction.toInt())
            .build()
    }

    fun mapLocalDateTime(value: LocalDateTime): String {
        return value.toString() // TODO: Correct?
    }

    fun mapLocalDateTimeOptional(value: LocalDateTime): StringValue {
        return mapToStringValue(mapLocalDateTime(value))
    }

    fun mapLocalDate(value: LocalDate): String {
        return value.toString() // TODO: Correct?
    }

    fun mapLocalDateOptional(value: LocalDate): StringValue {
        return mapToStringValue(mapLocalDate(value))
    }

    fun mapYearMonth(value: YearMonth): String {
        return value.toString() // TODO: Correct?
    }

    fun mapYearMonthOptional(value: YearMonth): StringValue {
        return mapToStringValue(mapYearMonth(value))
    }

    fun mapYear(value: Year): String {
        return value.toString() // TODO: Correct?
    }

    fun mapYearOptional(value: Year): StringValue {
        return mapToStringValue(mapYear(value))
    }

    fun mapAny(value: kotlin.Any): Any {
        return if (value is Any) {
            value
        } else {
            val localAnyHandler = anyHandler
                ?: throw IllegalArgumentException("No mapping possible from ${value::class} to ${Any::class}")

            localAnyHandler(value)
        }
    }

    fun registerAnyHandler(anyHandler: KotlinToProtoAnyHandler) {
        this.anyHandler = anyHandler
    }
}