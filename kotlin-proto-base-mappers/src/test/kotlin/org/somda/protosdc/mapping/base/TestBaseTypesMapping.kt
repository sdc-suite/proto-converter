package org.somda.protosdc.mapping.base

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.microseconds
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.nanoseconds
import kotlin.time.Duration.Companion.seconds

class TestBaseTypesMapping {
    @Test
    fun `test duration round trip`() {
        val durations = listOf(
            30.seconds,
            500.milliseconds,
            999999999.nanoseconds,
            0.minutes,
            5.minutes + 500.nanoseconds,
            3.seconds + 300.microseconds,
            7.hours + 56.minutes + 78.seconds + 100.milliseconds + 12000.nanoseconds,
            2.days,
            893473849383.microseconds
        )

        durations.forEach {
            assertEquals(it, ProtoToKotlinBaseTypes.mapDuration(KotlinToProtoBaseTypes.mapDuration(it)))
        }
    }
}