package org.somda.protosdc.mapping.base

import org.apache.logging.log4j.kotlin.Logging
import org.w3c.dom.Attr
import org.w3c.dom.Element
import org.w3c.dom.Node
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Year
import java.time.YearMonth
import javax.xml.XMLConstants
import javax.xml.namespace.QName
import kotlin.time.Duration

typealias KotlinToXmlAnyHandler = (value: kotlin.Any, parent: Element) -> Element

@Suppress("unused", "MemberVisibilityCanBePrivate")
object KotlinToXmlBaseTypes : Logging {
    private var anyHandler: KotlinToXmlAnyHandler? = null

    fun mapAnyURI(value: String, node: Node): Node {
        node.textContent = value
        return node
    }

    fun mapBoolean(value: Boolean, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapDate(value: LocalDate, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapDateTime(value: LocalDateTime, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapDecimal(value: BigDecimal, node: Node): Node {
        node.textContent = value.toPlainString()
        return node
    }

    fun mapDuration(value: Duration, node: Node): Node {
        node.textContent = value.toIsoString()
        return node
    }

    fun mapGYearMonth(value: YearMonth, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapGYear(value: Year, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapInt(value: Int, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapInteger(value: Long, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapLanguage(value: String, node: Node): Node {
        node.textContent = value
        return node
    }

    fun mapLong(value: Long, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapQName(value: QName, node: Node): Node {
        val prefix = node.ownerDocument.lookupPrefix(value.namespaceURI) ?: setForFreePrefix(node, value.namespaceURI)
        node.textContent = "$prefix:${value.localPart}"
        return node
    }

    fun mapString(value: String, node: Node): Node {
        node.textContent = value
        return node
    }

    fun mapUnsignedInt(value: Int, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapUnsignedLong(value: Long, node: Node): Node {
        node.textContent = value.toString()
        return node
    }

    fun mapAnySimpleType(value: String, node: Node): Node {
        node.textContent = value
        return node
    }

    fun mapAny(value: kotlin.Any, parent: Element) {
        if (value is Element) {
            parent.appendChild(value)
        } else {
            val localAnyHandler = anyHandler
                ?: throw IllegalArgumentException("No mapping possible from ${value::class} to ${Element::class}")

            parent.appendChild(localAnyHandler(value, parent))
        }
    }

    fun registerAnyHandler(anyHandler: KotlinToXmlAnyHandler) {
        this.anyHandler = anyHandler
    }

    fun setForFreePrefix(node: Node, namespaceUri: String): String {
        var parent = if (node is Attr) {
            node.ownerElement
        } else {
            node.parentNode
        }

        while (parent !is Element && parent != null) {
            parent = parent.parentNode
        }

        if (parent == null) {
            parent = node.ownerDocument.firstChild!!
        }

        // do not use ns prefix since it is used by the w3c dom writer, which does not expose the use to the user
        val prefix = "n"
        var number = 0
        while (parent.lookupNamespaceURI("$prefix$number") != null) {
            number++
        }

        if (parent is Element) {
            parent.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:$prefix$number", namespaceUri)
        } else {
            throw Exception("No element parent found for node $node")
        }

        return "$prefix$number"
    }
}